<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

Route::get('/', 'HomeController@welcome');
Route::post("loginpassword/",'Auth\LoginController@loginfull');
Route::get('service/changestatus/{id}','ServicesController@changeStatus');
Route::post('ajaxturnoactual','HomeController@TurnoActual');
Route::post('ajaxpersonalturno','HomeController@PersonalTurno');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('personal', 'FuntionaryController');
	Route::resource('services', 'ServicesController');
	Route::resource('novedades', 'NoveltyController');
	Route::resource('denuncias', 'DenunciasController');
	Route::resource('turnos', 'TurnosController');
	Route::resource('personas', 'PersonasController');
	Route::resource('arrestados', 'ArrestadosController');
	Route::resource('motos', 'MotosController');
	Route::post('avatar/','FuntionaryController@avatar');
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes

});

Auth::routes();

Route::get('/home', 'HomeController@index');

