<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Novelty extends Model
{
    use SoftDeletes;

	public function service()
	{
	    return $this->belongsTo('App\Service', 'service_id');
	}
	public function encargado()
	{
	    return $this->belongsTo('App\Funtionary', 'encargado_id');
	}
	public function funtionary()
	{
	    return $this->belongsTo('App\Funtionary', 'funtionary_id');
	}
	public function noveltyassistant(){
        return $this->belongsToMany('\App\Funtionary','assistants')
               ->withPivot('funtionary_id');
    }
    public function personasarrestadas(){
        return $this->belongsToMany('\App\Persona','arresteds')
               ->withPivot('persona_id');
    }

    protected $dates = [
            'hora_inicio', 
            'hora_final',
            'fecha',
            'created_at',
            'updated_at',
            'deleted_at',
        ];

    public function scopeFecha($query,$date)
    {
        return $query->whereDate('fecha', '=',$date);
    }
    public function scopeMes($query,$date)
    {
    	$date=explode('-',$date);
        return $query->whereMonth('fecha', '=',$date[1])->whereYear('fecha', '=', $date[0]);
    }
     public function scopeAnual($query,$date)
    {
    	$date=explode('-',$date);
        return $query->whereYear('fecha', '=', $date[0]);
    }


    //accesores y mutadores
    public function getNumberAttribute($value)
        {
            return str_pad((int) $this->id,6,"0",STR_PAD_LEFT);
        }
    
}
