<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denuncia extends Model
{
    //

    public function denunciados(){
    	return $this->hasMany('App\Denunciado','denuncia_id','id');
    }
}
