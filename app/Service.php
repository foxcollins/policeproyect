<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $dates = [
	        'created_at', 
	        'updated_at',
	    ];
    
    public function novelties()
        {
            return $this->hasMany('App\Novelty', 'service_id', 'id');
        }
}
