<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motos extends Model
{
    protected $dates = [
            'hora', 
            'fecha',
            'created_at',
            'updated_at',
        ];
    public function propietario()
    {
        return $this->belongsTo('App\Persona', 'propietario_id');
    }
}
