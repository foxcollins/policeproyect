<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
  	public function moto(){
  		return $this->hasOne('App\Motos','propietario_id','id');
  	}
    public function arrestados(){
        return $this->belongsToMany('\App\Novelty','arresteds')
               ->withPivot('novelty_id');
    }
}
