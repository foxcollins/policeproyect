<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funtionary extends Model
{
    protected $dates = [
            'graduado', 
            'inicio',
            'nacimiento',
            'created_at',
            'updated_at',
        ];

    public function user(){
    	return $this->hasOne('App\User','funtionary_id','id');
    }
    public function novelty(){
    	return $this->hasOne('App\Novelty','encargado_id','id');
    }

    public function novedad(){
    	return $this->hasOne('App\Novelty','functionary_id','id');
    }
    public function assistants(){
        return $this->belongsToMany('\App\Novelty','assistants')
               ->withPivot('novelty_id');
    }
    public function turno(){
        return $this->belongsToMany('\App\Turno','funtionary_turnos')
        ->withPivot('turno_id'); 
    }
 


     public function scopeFiltro($query,$date)
    {
        if (!empty($date)) {
            return $query->where('name', 'like', '%' .$date. '%')
            ->orWhere('lastname', 'like', '%' . $date. '%')
            ->orWhere('cedula', 'like', '%' .$date. '%');
        }
    }
    
}
