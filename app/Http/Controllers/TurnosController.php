<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turno;
use App\Funtionary;
use App\Http\Requests\TurnoRequest;
use Carbon\Carbon;
use Alert;
class TurnosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => 'welcome']);
        Carbon::setLocale('es'); 
        setlocale(LC_TIME, 'Spanish');
        setlocale(LC_TIME, 'es_VE', 'es_VE.utf-8', 'es_VE.utf8'); # Asi es mucho mas seguro que funciones, ya que no todos los sistemas llaman igual al locale ;)
        date_default_timezone_set('America/Caracas');
    }
    public function index()
    {
        $turnos=Turno::all();
        return view('adminlte::turnos.index',compact('turnos'));
    }

    
    public function create()
    {
        //
    }

    
    public function store(TurnoRequest $request)
    {
        //
        $now = Carbon::now();
        $turno=new Turno;
        $turno->title=$request->title;
        $turno->hora_inicial=Carbon::createFromFormat('Y-m-d H:i a', $now->format('Y-m-d')." ".$request->hora_inicial);
        $turno->hora_final=Carbon::createFromFormat('Y-m-d H:i a', $now->format('Y-m-d')." ".$request->hora_final);
        $turno->status=$request->status;
        if ($turno->status=="") {
            $turno->status="inactive";
        }
        if ($turno->save()) {
            Alert::success('Turno Agregado exitosamente','Turno Creado');
           return redirect('/turnos');
        }else{
            Alert::error('Algo salio mal, vuelva a intentarlo nuevamente','Opps');
            return redirect()->back()->withInputs();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //
        if ($request->ajax()) {
            $turno=Turno::find($id);
            return response()->json($turno);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->ajax()) {
            $turno=Turno::find($request->id);
            $turno->title=$request->title;
            $now = Carbon::now();
            $turno->title=$request->title;
            $turno->hora_inicial=Carbon::createFromFormat('Y-m-d H:i a', $now->format('Y-m-d')." ".$request->hora_inicial);
            $turno->hora_final=Carbon::createFromFormat('Y-m-d H:i a', $now->format('Y-m-d')." ".$request->hora_final);
            $turno->status=$request->status;
            if ($turno->save()) {
                $data='success';
                return response()->json($data);
            }
            else{
                $data='error';
                return response()->json($data);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
