<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AvatarRequest;
use App\Funtionary;
use App\User;
use Carbon\Carbon;
class FuntionaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => 'welcome']);
        Carbon::setLocale('es'); 
        setlocale(LC_TIME, 'Spanish');
        setlocale(LC_TIME, 'es_ES');
        
    }

    public function index(Request $request)
    {
        
        if (!empty($buscar=\Request::get('buscar'))) {
           $funtionaries=Funtionary::filtro($buscar)->where('status','active')->where('rango','<>','admin')->paginate(10);
        }
        else{
            $funtionaries=Funtionary::where('status','active')->where('rango','<>','admin')->paginate(10);
        }
        return view("adminlte::funtionary.index",compact('funtionaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $funtionary=Funtionary::find($id);
        return view("adminlte::funtionary.show",compact('funtionary'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function avatar(AvatarRequest $request){
        $funtionary=Funtionary::find($request->id);
        $mime = $request->file('avatar')->getMimeType();
        $extension = strtolower($request->file('avatar')->getClientOriginalExtension());
        $fileName = $request->id.'.'.$extension;

        $path = "img/avatar";
        $funtionary->avatar=$fileName;
        if ($funtionary->save()) {
            $request->file('avatar')->move($path, $fileName);
            return redirect()->back();
        }
        

        return 'avatar:'.$fileName;
    }
}
