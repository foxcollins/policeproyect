<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motos;
use Carbon\Carbon;
use Alert;

class MotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $moto= new Motos;
        $moto->documento=$request->documento;
        $moto->licencia=$request->licencia;
        $moto->certificado=$request->certificado;
        $moto->casco=$request->casco;
        $moto->serial_motor=$request->serial_motor;
        $moto->serial_carroceria=$request->serial_carroceria;
        $moto->placa=$request->placa;
        $moto->conductor_id=$request->conductor_id;
        $moto->propietario_id=$request->propietario_id;
        $moto->novelty_id=$request->novelty_id;
        $moto->marca=$request->marca;
        $moto->modelo=$request->modelo;
        $moto->observacion=$request->observacion;
        $moto->year=$request->year;
        $moto->fecha=Carbon::createFromFormat('Y-m-d', $request->fecha);
        $moto->hora=Carbon::createFromFormat('Y-m-d H:i a', $request->fecha." ".$request->hora);
        foreach ($request->colores as $color) {
            $moto->colores=str_slug($moto->colores." ".$color,',');
        }
        if ($moto->save()) {
            Alert::success('Moto Agregda con exito, Ahora complete el siguiente paso para finalizar','Moto Agregada');
            return Redirect('motos/'.$moto->id)->with($request->novelty_id);
        }
        else{
            Alert::error('Fallo la operación, Vuelva a intentarlo nuevamente','Fallo la Operación');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $moto=Motos::find($id);
        return view('adminlte::motos.show',compact('moto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
