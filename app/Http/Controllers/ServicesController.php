<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Alert;
use Carbon\Carbon;
use  App\Http\Requests\ServiceRequest;
class ServicesController extends Controller
{
   
    public function index()
    {
        $services=Service::all();
        return view('adminlte::services.index',compact('services'));
    }

   
    public function create()
    {
        //
    }

 
    public function store(ServiceRequest $request)
    {
        $service= new Service;
        $service->title=$request->title;
        $service->status='active';
        if ($service->save()) {
            
            Alert::success('Servicio Registrado','Se agrego un nuevo servicio al sistema');
            return redirect('services');
        }else{
            Alert::error('Servicio Registrado','Se agrego un nuevo servicio al sistema');
            return redirect('services');
        }
    }

 
    public function show(Request $request, $id)
    {
        //
        if ($request->ajax()) {
            $data=Service::find($id);
            return Response()->json($data);
        }
    }

   
    public function edit($id)
    {
        //
    }

 
    public function update(Request $request, $id)
    {
        $service=Service::find($request->id);
        $service->title=$request->title;
        if ($service->save()) {
            Alert::success('Servicio actualizado con exito','Servicio actualizado');
            return redirect('services');
        }else{
            Alert::warning('No se pudo Actualizar el registro','Error al actualizar');
            return redirect('services');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatus(Request $request,$id){
        if ($request->ajax()) {
            $service=Service::find($request->id);
            if ($service->status=="active") {
                $service->status='inactive';
            }
            else{
                $service->status='active';
            }
            $service->save();
            Alert::success('estado cambiado correctamente','Se cambio el estado')->autoclose(1500);
            return response()->json();
        }
    }
}
