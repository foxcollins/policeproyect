<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Arrested;
use Carbon\Carbon;
use Alert;

class ArrestadosController extends Controller
{
    
    public function index()
    {
        //
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
        $arrestado=new Arrested;
        $arrestado->status=$request->status;
        $arrestado->resumen=$request->resumen;
        $arrestado->persona_id=$request->persona_id;
        $arrestado->funtionary_id=Auth::user()->id;
        $arrestado->novelty_id=$request->novelty_id;
        if ($arrestado->save()) {
            $request->session()->forget('modal');
            $request->session()->forget('id');
            $request->session()->forget('cedula');
            $request->session()->forget('name');
            $request->session()->forget('lastname');
            $request->session()->forget('selectId');
            Alert::success('Arresto agregado con exito','Arresto agregado');
            return redirect()->back();
        }

    }

    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
