<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Funtionary;
use App\funtionaryTurno;
use App\Novelty;
use App\Turno;
use Alert;
use Carbon\Carbon;
class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth',['except' => 'welcome']);
        Carbon::setLocale('es'); 
        setlocale(LC_TIME, 'Spanish');
        setlocale(LC_TIME, 'es_VE', 'es_VE.utf-8', 'es_VE.utf8'); # Asi es mucho mas seguro que funciones, ya que no todos los sistemas llaman igual al locale ;)
        date_default_timezone_set('America/Caracas');
    }

    
    public function index(Request $request)
    {
        
        if (Auth::check()) {
           
            //$novedadesmes=\DB::select("select * from novelties where month(fecha)=month(NOW())");
            //$mes=date('m');
            $year=date('Y');
            $m=Carbon::now();
            $mes=$m->month;
            $mesanterior=$m->subMonth(1)->month;
            $novedadesmes=Novelty::whereMonth('fecha', '=', $mes)->whereYear('fecha',$year)->count();
            $concretadasmes=Novelty::whereMonth('fecha', '=', $mesanterior)->whereYear('fecha',$year)->count();
            if ($concretadasmes<1) {
                $concretadas=0;
            }else{
                $concretadas=($novedadesmes-$concretadasmes)/$concretadasmes;
            }
            return view('vendor.adminlte.home',compact('novedadesmes','concretadas'));
        }else{
           return view('adminlte::layouts.landing');
        }
        
    }
    public function welcome()
    {
        if (Auth::check()) {
           #\Alert::success(Auth::user()->name." ".Auth::user()->lastname , 'Bienvenido');
            //$novedadesmes=\DB::select("select * from novelties where month(fecha)=month(NOW())")->count();
            //return view('home',compact('novedadesmes'));
            return view('adminlte::home');
        }
        return view('adminlte::layouts.landing');
    }

    public function TurnoActual(Request $request){
        $horaactual=date('H:i:s');
        if ($request->ajax()) {
             $turno=Turno::whereTime('hora_inicial', '<=',$horaactual)
             ->whereTime('hora_final', '>=',$horaactual)
             ->where('status','=','active')->get();
             
            return response()->json($turno);
        }
    }
    public function PersonalTurno(Request $request){
        $today=date('Y-m-d');
        $horaactual=date('H:i:s');
        if ($request->ajax()) {
             $personal=funtionaryTurno::select('funtionaries.*')
             ->where('turno_id','=',$request->id)->where('active','=','true')->whereDate('fecha_inicio','<=',$today)->whereDate('fecha_fin','>=',$today)
             ->join('funtionaries','funtionary_turnos.funtionary_id','=','funtionaries.id')->get();

            return response()->json($personal);
        }
    }
}
