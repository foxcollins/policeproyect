<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use Alert;
use Carbon\Carbon;

class PersonasController extends Controller
{
    
    public function index($id)
    {
        //
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
        $persona=new Persona;
        $persona->cedula=$request->cedula;
        $persona->name=$request->name;
        $persona->lastname=$request->lastname;
        $persona->sexo=$request->sexo;
        $persona->direccion=$request->direccion;
        $persona->fecha_nacimiento=$request->fecha_nacimiento;
        $persona->save();
        $modalId=$request->modal;
        $request->session()->put('modal', $modalId);
        $request->session()->put('id', $persona->id);
        $request->session()->put('cedula', $persona->cedula);
        $request->session()->put('name', $persona->name);
        $request->session()->put('lastname', $persona->lastname);
        $request->session()->put('selectId',$request->selectId);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
