<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Session;
use Redirect;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Funtionary;
use App\User;
use Alert;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

   // use AuthenticatesUsers;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('adminlte::layouts.landing');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request){
        if ($request->ajax()) {
            $cedula=$request->cedula;
            $funtionary=Funtionary::where('cedula','=',$cedula)->first();
            if (count($funtionary)>0) {
                //si existe la cedula
                $user=User::where('funtionary_id','=',$funtionary->id)->first();
                if (count($user)>0) {
                    #existe el usuario
                    if ($user->status=="bloqueado") {
                        $resp="bloqueado";
                        return Response()->json($resp);
                    }else{
                        $resp="success";
                        return Response()->json($resp);
                    }
                }else{
                    #no tiene usuario
                    $resp="nouser";
                    return Response()->json($resp);
                }
            }else{
                //no existe la cedula en la BD
                $resp="noexiste";
                //Alert::warning("la credencial ".$cedula , 'no existen en nuestros registro');
                return Response()->json($resp);
            }
            
        }
    }

    public function loginfull(Request $request){
        if ($request->ajax()) {
            $credencial=$request->cedula;
            $password=$request->password;
            if (Auth::attempt(['credencial' => $credencial , 'password' => $password,'status'=>'active'])) {
                #usuario autenticado
                //return Redirect()->intented('/home');
                $user=User::find(Auth::user()->id);
                $user->intentos=0;
                $user->save();
                $resp="success";
                Alert::success('Ingeso correctamente al sistem','Bienvenido');
                return Response()->json($resp);
            }
            else{
                $user=User::where('credencial','=',$credencial)->first();
                $user->intentos=($user->intentos+1);
                if ($user->intentos>2) {
                    $user->status="bloqueado";
                }
                $user->save();
                $resp=['error'=>'error','int'=>$user->intentos];
               
               return Response()->json($resp);
            }
            
        }
    }

    public function logout(Request $request){
        Auth::logout();
        Alert::message('Sesión finalizada con exito  <i class="fa fa-check fa-lg"> </i>','Logout')->html()->autoclose(6500);
        return redirect::to('/');
    }
}
