<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NoveltyRequest;
use App\Novelty;
use App\Service;
use App\Funtionary;
use App\Assistant;
use App\Persona;
use App\Arrested;
use App\Motos;
use Carbon\Carbon;
use Auth;
use Alert;
class NoveltyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => 'welcome']);
        Carbon::setLocale('es');

        setlocale(LC_TIME, 'Spanish');
        setlocale(LC_TIME, 'es_ES');
        

        
    }

    public function index(Request $request)
    {   
        ##whereMonth('fecha', '=', \Carbon\Carbon::now()->format('m'))
        ##->whereYear('fecha', '=', Carbon::now()->format('Y')
        if (!empty($request->type)) {
            if ($request->get('type')=='dia') {
            $novedades=Novelty::fecha($request->get('data'))->orderBy('fecha','DESC')->paginate(5);  
            }
            if ($request->get('type')=='mes') {
            $novedades=Novelty::mes($request->get('data'))->orderBy('fecha','DESC')->paginate(5);  
            }
            if ($request->get('type')=='anual') {
            $novedades=Novelty::anual($request->get('data'))->orderBy('fecha','DESC')->paginate(5);  
            }
            
        }else{
            $novedades=Novelty::orderBy('fecha','DESC')->paginate(5);
        }
        $countnovedades=Novelty::whereMonth('fecha', '=', \Carbon\Carbon::now()->format('m'))
        ->whereYear('fecha', '=', Carbon::now()->format('Y'))
        ->orderBy('fecha','DESC')->get();
        return view('adminlte::novedades.index',compact("novedades","countnovedades"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $services=Service::all();
        $funtionaries=Funtionary::where('rango','<>','admin')->get();
        return view('adminlte::novedades.create',compact('services','funtionaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoveltyRequest $request)
    {
        $novedad= new Novelty;
        //dd(strtotime($request->horainicial));
        $novedad->hora_inicio=Carbon::createFromFormat('Y-m-d H:i a', $request->fecha." ".$request->horainicial);
        $novedad->hora_final=Carbon::createFromFormat('Y-m-d H:i a', $request->fecha." ".$request->horafinal);
        $novedad->status="activa";
        $novedad->fecha=Carbon::createFromFormat('Y-m-d', $request->fecha);
        $novedad->resumen = $request->resumen;
        $novedad->lugar = $request->lugar;
        $novedad->personas = $request->personas;
        $novedad->motos = $request->motos;
        $novedad->autos = $request->autos;
        $novedad->encargado_id = $request->encargado;
        $novedad->funtionary_id=Auth::user()->id;
        $novedad->service_id=$request->service;
        if ($novedad->save()) {
            # guardo los auxiliares
            if (count($request->auxiliares)>0) {
                foreach ($request->auxiliares as $auxiliar) {
                    $aux= new Assistant;
                    $aux->novelty_id=$novedad->id;
                    $aux->funtionary_id=$auxiliar;
                    $aux->save();
                }
            }
            Alert::success('Recuerde completar los datos de personas o vehículos detenidos','Creada con exito');
            return redirect('novedades/'.$novedad->id);
        }else{
            Alert::danger('Error Message', 'Optional Title');
            return redirect('/novedades');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $novelty=Novelty::find($id);
        $personas=Persona::all();
        $arrestos=Arrested::where('novelty_id','=',$id)
        ->join('personas','arresteds.persona_id','=','personas.id')->get();
        $motos=Motos::where('novelty_id','=',$id)->get();
        return view('adminlte::novedades.show',compact('novelty','personas','arrestos','motos'));
    }

  
    public function edit($id)
    {
        //
        $novelty=Novelty::find($id);
        $services=Service::all();
        $funtionaries=Funtionary::all();
        return view('adminlte::novedades.edit',compact('novelty','services','funtionaries'));
    }

  
    public function update(Request $request, $id)
    {
        //
        $novedad=Novelty::find($request->id);
        //dd(strtotime($request->horainicial));
        $novedad->hora_inicio=Carbon::createFromFormat('Y-m-d H:i a', $request->fecha." ".$request->horainicial);
        $novedad->hora_final=Carbon::createFromFormat('Y-m-d H:i a', $request->fecha." ".$request->horafinal);
        $novedad->status="activa";
        $novedad->fecha=Carbon::createFromFormat('Y-m-d', $request->fecha);
        $novedad->resumen = $request->resumen;
        $novedad->lugar = $request->lugar;
        $novedad->personas = $request->personas;
        $novedad->motos = $request->motos;
        $novedad->autos = $request->autos;
        $novedad->encargado_id = $request->encargado;
        $novedad->funtionary_id=Auth::user()->id;
        $novedad->service_id=$request->service;
        if ($novedad->save()) {
            # guardo los auxiliares
            if (count($request->auxiliares)>0) {
                $clear=Assistant::where('novelty_id','=',$id)->delete();
                foreach ($request->auxiliares as $auxiliar) {
                    $aux= new Assistant;//::where('novelty_id','=',$id)->where('funtionary_id','=',$auxiliar)->first());
                    $aux->novelty_id=$id;
                    $aux->funtionary_id=$auxiliar;
                    $aux->save();
                }
            }
            Alert::success('Novedad actualizada con exito','Actualizada');
            return redirect('novedades/'.$novedad->id);
        }else{
            Alert::error('Error Message', 'Optional Title');
            return redirect('/novedades');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $novelty=Novelty::find($id);
        $novelty->delete();
        Alert::warning('Novedad eliminada con exito','Novedad #'.$novelty->id.' Eliminada')->persistent('Close');
        return redirect('/novedades');
    }
}
