<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar'=>'required|image',
        ];
    }
    public function messages(){
        return[
            'avatar.image'=>'el formato debe ser jpeg, png, bmp o gif',
            'avatar.required'=>'Selecione una imagen para poder cargarla a su perfil, el campo no pude estar vacio',
            'avatar.size'=>'su imagen no pude exceder de 1 MB',
        ];
    }
}
