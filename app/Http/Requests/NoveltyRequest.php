<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoveltyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service'=>'required',
            'fecha'=>'required|date:d-m-YYYY',
            'horainicial'=>'required',
            'horafinal'=>'required',
            'lugar'=>'required|min:8|max:200',
            'resumen'=>'required|min:20|',
            'encargado'=>'required|exists:funtionaries,id',

        ];
    }

    public function messages()
    {
        return [
            'service.required' => 'indique a cual de los servicios pertenece esta novedad',
            'fecha.required' => 'Debe indicar la fecha',
            'horainicial.required' => 'Indique la hora de inicio del servicio',
            'horainicial.date' => 'Ingresa un formato valido para la hora inicial',
            'horafinal.required'=>'Indique la hora de culminación del servicio',
            'horafinal.date'=>'Ingresa un formato valido para la hora final',


        ];
    }
}
