<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TurnoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'hora_inicial'=>'required',
            'hora_final'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El campo titulo es requerido',
            'hora_inicial.required'   => 'Debe indicar una hora inicial',
            'hora_final.required'   => 'Debe indicar una hora final',
        ];
    }
}
