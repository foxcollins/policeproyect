<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'=>'required|unique:services,title', 
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El campo titulo es requerido',
            'title.unique'   => 'Este servicio ya existe en los registros'
        ];
    }
}
