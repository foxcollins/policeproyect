<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    //
    protected $dates = [
            'created_at', 
            'updated_at',
            'hora_inicial',
            'hora_final',
        ];

    public function funtionary(){
        return $this->belongsToMany('\App\Funtionary','funtionary_turnos')
        ->withPivot('funtionary_id'); 
    } 
}
