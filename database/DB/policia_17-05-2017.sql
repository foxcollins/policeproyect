-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 17, 2017 at 06:31 PM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `policia`
--

-- --------------------------------------------------------

--
-- Table structure for table `arresteds`
--

CREATE TABLE `arresteds` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resumen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `persona_id` int(10) UNSIGNED NOT NULL,
  `novelty_id` int(10) UNSIGNED NOT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `arresteds`
--

INSERT INTO `arresteds` (`id`, `status`, `resumen`, `persona_id`, `novelty_id`, `funtionary_id`, `created_at`, `updated_at`) VALUES
(1, 'departamento', '<p>ehfdfnsdfdsj sdfb sbdfhsdbhfbs hhdfbsdhfbdh hfhdbfuhdsbhb hdbghdbg hg&nbsp;</p>', 1, 1, 1, '2017-05-01 20:53:37', '2017-05-01 20:53:37'),
(2, 'departamento', '<p>esta es na observación del arresto</p>', 2, 1, 1, '2017-05-04 05:16:38', '2017-05-04 05:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `assistants`
--

CREATE TABLE `assistants` (
  `id` int(10) UNSIGNED NOT NULL,
  `novelty_id` int(10) UNSIGNED NOT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assistants`
--

INSERT INTO `assistants` (`id`, `novelty_id`, `funtionary_id`, `created_at`, `updated_at`) VALUES
(2, 1, 2, '2017-05-17 17:47:21', '2017-05-17 17:47:21'),
(3, 1, 3, '2017-05-17 17:47:21', '2017-05-17 17:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `autos`
--

CREATE TABLE `autos` (
  `id` int(10) UNSIGNED NOT NULL,
  `placa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licencia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clase` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_motor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_carroceria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `hora` datetime NOT NULL,
  `observacion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `propietario_id` int(10) UNSIGNED NOT NULL,
  `conductor_id` int(10) UNSIGNED NOT NULL,
  `novelty_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `citas`
--

CREATE TABLE `citas` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hora` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `denunciado_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `denunciados`
--

CREATE TABLE `denunciados` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `denuncia_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `denuncias`
--

CREATE TABLE `denuncias` (
  `id` int(10) UNSIGNED NOT NULL,
  `cedula` int(11) NOT NULL,
  `edad` int(11) NOT NULL,
  `estado_civil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `residencia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profesion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `funtionaries`
--

CREATE TABLE `funtionaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `cedula` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nacimiento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lugar_nacimiento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rango` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `madre_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `padre_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `graduado` datetime NOT NULL,
  `inicio` datetime NOT NULL,
  `estatura` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contextura` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `talla_zapato` int(11) NOT NULL,
  `talla_camisa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `talla_pantalon` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `funtionaries`
--

INSERT INTO `funtionaries` (`id`, `cedula`, `name`, `lastname`, `sexo`, `nacimiento`, `email`, `lugar_nacimiento`, `direccion`, `rango`, `avatar`, `status`, `madre_status`, `padre_status`, `graduado`, `inicio`, `estatura`, `contextura`, `talla_zapato`, `talla_camisa`, `talla_pantalon`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1234567, 'admin', 'master', 'masculino', '1990-01-01 04:00:00', 'admin@master.com', 'caicara', 'calle principal', 'admin', 'avatar-default.png', 'active', 'true', 'true', '2010-03-06 00:00:00', '2011-03-06 00:00:00', '1.80', 'mediana', 41, 'l', 34, '2017-05-01 20:48:57', '2017-05-01 20:48:57', NULL),
(2, 1234500, 'carlos', 'acosta', 'masculino', '1990-01-01 04:00:00', 'carlos@master.com', 'caicara', 'calle principal', 'comisionado jefe', 'avatar-default.png', 'active', 'true', 'false', '2010-03-06 00:00:00', '2011-03-06 00:00:00', '1.80', 'mediana', 42, 'm', 34, '2017-05-01 20:48:58', '2017-05-01 20:48:58', NULL),
(3, 1234501, 'petra', 'gomez', 'femenino', '1990-01-01 04:00:00', 'petra@master.com', 'caicara', 'calle principal', 'supervisor jefe', 'avatar-default.png', 'active', 'true', 'true', '2008-03-06 00:00:00', '2009-03-06 00:00:00', '1.80', 'mediana', 39, 's', 34, '2017-05-01 20:48:58', '2017-05-01 20:48:58', NULL),
(4, 1234568, 'juan', 'gomez', 'masculino', '1990-03-06 04:00:00', 'juan@gomez.com', 'maturin', 'calle 2', 'supervisor agregado', 'avatar-default.png', 'active', 'false', 'true', '2010-03-06 00:00:00', '2011-03-06 00:00:00', '1.80', 'mediana', 45, 'xl', 36, '2017-05-01 20:48:58', '2017-05-01 20:48:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `funtionary_turnos`
--

CREATE TABLE `funtionary_turnos` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `turno_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `incautados`
--

CREATE TABLE `incautados` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(39, '2017_03_07_034008_create_funtionaries_table', 1),
(40, '2017_03_07_034157_create_histories_table', 1),
(41, '2017_03_07_034241_create_services_table', 1),
(42, '2017_03_07_040702_create_phones_table', 1),
(43, '2017_03_07_041433_create_denuncias_table', 1),
(44, '2017_03_08_000000_create_users_table', 1),
(45, '2017_03_11_022257_create_denunciados_table', 1),
(46, '2017_03_11_022829_create_citas_table', 1),
(47, '2017_03_19_051354_create_novelties_table', 1),
(48, '2017_03_19_052056_create_incautados_table', 1),
(49, '2017_03_30_231813_create_assistants_table', 1),
(50, '2017_04_16_031815_create_turnos_table', 1),
(51, '2017_04_16_033929_create_funtionary_turnos_table', 1),
(52, '2017_04_27_185555_create_padres_table', 1),
(53, '2017_04_28_032032_create_personas_table', 1),
(54, '2017_04_28_033057_create_motos_table', 1),
(55, '2017_04_28_033113_create_autos_table', 1),
(56, '2017_04_28_041434_create_motos_accesorios_table', 1),
(57, '2017_04_28_051522_create_arresteds_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `motos`
--

CREATE TABLE `motos` (
  `id` int(10) UNSIGNED NOT NULL,
  `placa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `licencia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `casco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_motor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_carroceria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colores` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `hora` datetime NOT NULL,
  `observacion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `propietario_id` int(10) UNSIGNED NOT NULL,
  `conductor_id` int(10) UNSIGNED NOT NULL,
  `novelty_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `motos`
--

INSERT INTO `motos` (`id`, `placa`, `documento`, `licencia`, `certificado`, `casco`, `serial_motor`, `serial_carroceria`, `marca`, `modelo`, `year`, `colores`, `fecha`, `hora`, `observacion`, `propietario_id`, `conductor_id`, `novelty_id`, `created_at`, `updated_at`) VALUES
(6, '9G6H3', 'si', 'no', 'no', 'no', '232312U12312312UG', 'HJ43423J2342-23424', 'HONDA', '100 clasic', '1993', 'rojo', '2019-08-03 02:32:36', '2019-08-03 10:05:00', '<p>sdfsdffdsf sdfdsfdsfd dsfsd sdf wewef sdf&nbsp;</p>', 2, 2, 1, '2017-05-04 06:32:36', '2017-05-04 06:32:36');

-- --------------------------------------------------------

--
-- Table structure for table `motos_accesorios`
--

CREATE TABLE `motos_accesorios` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `moto_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `novelties`
--

CREATE TABLE `novelties` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lugar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resumen` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personas` int(11) NOT NULL,
  `motos` int(11) NOT NULL,
  `autos` int(11) NOT NULL,
  `hora_inicio` datetime NOT NULL,
  `hora_final` datetime NOT NULL,
  `fecha` datetime NOT NULL,
  `encargado_id` int(10) UNSIGNED NOT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `novelties`
--

INSERT INTO `novelties` (`id`, `status`, `lugar`, `resumen`, `personas`, `motos`, `autos`, `hora_inicio`, `hora_final`, `fecha`, `encargado_id`, `funtionary_id`, `service_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'activa', 'entrada de caicara', '<p>dshfbh bhsdbhbhbhsdbf bhbhbhbshdfb hdfbhsdfbsdhfbhsbdfhsdbfhsdbfhfhdsb hfbhdsfb hsdbfhdsfb hdfbsdhfbdsh bdshfb sdhfbsdf bdshfb hfbhbhsdbf hbsdfhsdbhfbsdhf hsffabdofbadsofubadofu baofbuafbsdhf bahdfbsdahfbasdhfb hdasfbdahfbh sfbhsdabfhasdbfh bsfhsdbfdshfbasdhfb hdfbahsdfbhsdaafb hdfbhsadafbhsdfbh g3ew8of g3y8gq38y g3ry838rb y8r38r beb 8ye fehbhufd befvbfudhvbdf vuhbvu hfdbvhufdbvdf vuhfdb uhdfvbuhf uhfdb uhfvbuhfd hfdvbhufdbv hfdbvhudbvuhfdb uhfdbvudhfbvudfh vd dn dfv dfvhdvdsfvbjdfvfdj uinfudivh difvh 8fuvusvdhufbv hfdb hfbv hdf hfdb hfdvbsfuhdvb uhdhsdb vjbewberibge vfehvbfdjbvehfbhrebvfhdvb bufeb ebv fehvudfb oebv ofebvoehf&nbsp;</p>', 7, 3, 6, '2017-05-17 20:00:00', '2017-05-17 20:00:00', '2017-05-17 13:47:21', 4, 1, 3, '2017-05-01 20:51:19', '2017-05-17 17:47:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `padres`
--

CREATE TABLE `padres` (
  `id` int(10) UNSIGNED NOT NULL,
  `cedula` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` datetime NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `id` int(10) UNSIGNED NOT NULL,
  `cedula` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `name`, `lastname`, `sexo`, `direccion`, `fecha_nacimiento`, `created_at`, `updated_at`) VALUES
(1, 33333333, 'juanita', 'jimenez', 'Femenino', 'calle caicara #2', '1984-04-06 00:00:00', '2017-05-01 20:53:17', '2017-05-01 20:53:17'),
(2, 55555555, 'Juan', 'Perez', 'Masculino', 'calle caicara #2', '1990-04-05 00:00:00', '2017-05-04 05:15:51', '2017-05-04 05:15:51');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` int(11) NOT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Patrullaje Motorizado', 'active', NULL, NULL),
(2, 'Punto de Control', 'active', NULL, NULL),
(3, 'Servicio Punto a Pie', 'active', NULL, NULL),
(4, 'Patrullaje en unidad 4-112', 'active', NULL, NULL),
(5, 'Servicio en Organismo de salud', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `turnos`
--

CREATE TABLE `turnos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hora_inicial` datetime NOT NULL,
  `hora_final` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `turnos`
--

INSERT INTO `turnos` (`id`, `title`, `hora_inicial`, `hora_final`, `status`, `created_at`, `updated_at`) VALUES
(1, 'primer turno', '2017-05-17 18:00:00', '2017-05-17 00:00:00', 'active', '2017-05-17 13:29:51', '2017-05-17 13:29:51'),
(2, 'segundo turno', '2017-05-17 00:00:00', '2017-05-17 03:00:00', 'active', '2017-05-17 13:31:36', '2017-05-17 13:31:36'),
(3, 'prueba', '2017-05-17 06:00:00', '2017-05-17 12:00:00', 'active', '2017-05-17 13:34:39', '2017-05-17 13:37:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pregunta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `respuesta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credencial` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intentos` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `funtionary_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `status`, `pregunta`, `respuesta`, `credencial`, `password`, `intentos`, `remember_token`, `funtionary_id`, `created_at`, `updated_at`) VALUES
(1, 'active', '¿cual fue tu primera mascota?', 'perro', '1234567', '$2y$10$Mt9LbJYS6dcKkZ8egC633e7cJXNhEV3HEInJPZMubw.3c3q.q6OXO', 0, 'BWL8LHYqNibQjN9O6YHfwNBs1YFJsX3hxd5wGrUHBGQy2xhTsoGnHFMbUx0k', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arresteds`
--
ALTER TABLE `arresteds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `arresteds_persona_id_foreign` (`persona_id`),
  ADD KEY `arresteds_novelty_id_foreign` (`novelty_id`),
  ADD KEY `arresteds_funtionary_id_foreign` (`funtionary_id`);

--
-- Indexes for table `assistants`
--
ALTER TABLE `assistants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assistants_novelty_id_foreign` (`novelty_id`),
  ADD KEY `assistants_funtionary_id_foreign` (`funtionary_id`);

--
-- Indexes for table `autos`
--
ALTER TABLE `autos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `autos_propietario_id_foreign` (`propietario_id`),
  ADD KEY `autos_conductor_id_foreign` (`conductor_id`),
  ADD KEY `autos_novelty_id_foreign` (`novelty_id`);

--
-- Indexes for table `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `citas_denunciado_id_foreign` (`denunciado_id`);

--
-- Indexes for table `denunciados`
--
ALTER TABLE `denunciados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `denunciados_denuncia_id_foreign` (`denuncia_id`);

--
-- Indexes for table `denuncias`
--
ALTER TABLE `denuncias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `denuncias_funtionary_id_foreign` (`funtionary_id`);

--
-- Indexes for table `funtionaries`
--
ALTER TABLE `funtionaries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `funtionaries_cedula_unique` (`cedula`);

--
-- Indexes for table `funtionary_turnos`
--
ALTER TABLE `funtionary_turnos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `funtionary_turnos_funtionary_id_foreign` (`funtionary_id`),
  ADD KEY `funtionary_turnos_turno_id_foreign` (`turno_id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `histories_funtionary_id_foreign` (`funtionary_id`),
  ADD KEY `histories_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `incautados`
--
ALTER TABLE `incautados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motos`
--
ALTER TABLE `motos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `motos_propietario_id_foreign` (`propietario_id`),
  ADD KEY `motos_conductor_id_foreign` (`conductor_id`),
  ADD KEY `motos_novelty_id_foreign` (`novelty_id`);

--
-- Indexes for table `motos_accesorios`
--
ALTER TABLE `motos_accesorios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `motos_accesorios_moto_id_foreign` (`moto_id`);

--
-- Indexes for table `novelties`
--
ALTER TABLE `novelties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `novelties_encargado_id_foreign` (`encargado_id`),
  ADD KEY `novelties_funtionary_id_foreign` (`funtionary_id`),
  ADD KEY `novelties_service_id_foreign` (`service_id`);

--
-- Indexes for table `padres`
--
ALTER TABLE `padres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `padres_funtionary_id_foreign` (`funtionary_id`);

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phones_funtionary_id_foreign` (`funtionary_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_funtionary_id_foreign` (`funtionary_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arresteds`
--
ALTER TABLE `arresteds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `assistants`
--
ALTER TABLE `assistants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `autos`
--
ALTER TABLE `autos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `denunciados`
--
ALTER TABLE `denunciados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `denuncias`
--
ALTER TABLE `denuncias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `funtionaries`
--
ALTER TABLE `funtionaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `funtionary_turnos`
--
ALTER TABLE `funtionary_turnos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `incautados`
--
ALTER TABLE `incautados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `motos`
--
ALTER TABLE `motos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `motos_accesorios`
--
ALTER TABLE `motos_accesorios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `novelties`
--
ALTER TABLE `novelties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `padres`
--
ALTER TABLE `padres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `turnos`
--
ALTER TABLE `turnos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `arresteds`
--
ALTER TABLE `arresteds`
  ADD CONSTRAINT `arresteds_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`),
  ADD CONSTRAINT `arresteds_novelty_id_foreign` FOREIGN KEY (`novelty_id`) REFERENCES `novelties` (`id`),
  ADD CONSTRAINT `arresteds_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`);

--
-- Constraints for table `assistants`
--
ALTER TABLE `assistants`
  ADD CONSTRAINT `assistants_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assistants_novelty_id_foreign` FOREIGN KEY (`novelty_id`) REFERENCES `novelties` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `autos`
--
ALTER TABLE `autos`
  ADD CONSTRAINT `autos_conductor_id_foreign` FOREIGN KEY (`conductor_id`) REFERENCES `personas` (`id`),
  ADD CONSTRAINT `autos_novelty_id_foreign` FOREIGN KEY (`novelty_id`) REFERENCES `novelties` (`id`),
  ADD CONSTRAINT `autos_propietario_id_foreign` FOREIGN KEY (`propietario_id`) REFERENCES `personas` (`id`);

--
-- Constraints for table `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_denunciado_id_foreign` FOREIGN KEY (`denunciado_id`) REFERENCES `denunciados` (`id`);

--
-- Constraints for table `denunciados`
--
ALTER TABLE `denunciados`
  ADD CONSTRAINT `denunciados_denuncia_id_foreign` FOREIGN KEY (`denuncia_id`) REFERENCES `denuncias` (`id`);

--
-- Constraints for table `denuncias`
--
ALTER TABLE `denuncias`
  ADD CONSTRAINT `denuncias_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`);

--
-- Constraints for table `funtionary_turnos`
--
ALTER TABLE `funtionary_turnos`
  ADD CONSTRAINT `funtionary_turnos_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`),
  ADD CONSTRAINT `funtionary_turnos_turno_id_foreign` FOREIGN KEY (`turno_id`) REFERENCES `turnos` (`id`);

--
-- Constraints for table `histories`
--
ALTER TABLE `histories`
  ADD CONSTRAINT `histories_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `funtionaries` (`id`),
  ADD CONSTRAINT `histories_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `motos`
--
ALTER TABLE `motos`
  ADD CONSTRAINT `motos_conductor_id_foreign` FOREIGN KEY (`conductor_id`) REFERENCES `personas` (`id`),
  ADD CONSTRAINT `motos_novelty_id_foreign` FOREIGN KEY (`novelty_id`) REFERENCES `novelties` (`id`),
  ADD CONSTRAINT `motos_propietario_id_foreign` FOREIGN KEY (`propietario_id`) REFERENCES `personas` (`id`);

--
-- Constraints for table `motos_accesorios`
--
ALTER TABLE `motos_accesorios`
  ADD CONSTRAINT `motos_accesorios_moto_id_foreign` FOREIGN KEY (`moto_id`) REFERENCES `motos` (`id`);

--
-- Constraints for table `novelties`
--
ALTER TABLE `novelties`
  ADD CONSTRAINT `novelties_encargado_id_foreign` FOREIGN KEY (`encargado_id`) REFERENCES `funtionaries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `novelties_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `novelties_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `padres`
--
ALTER TABLE `padres`
  ADD CONSTRAINT `padres_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`);

--
-- Constraints for table `phones`
--
ALTER TABLE `phones`
  ADD CONSTRAINT `phones_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_funtionary_id_foreign` FOREIGN KEY (`funtionary_id`) REFERENCES `funtionaries` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
