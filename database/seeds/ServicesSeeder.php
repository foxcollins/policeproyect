<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('services')->insert([
        	['title' => 'Patrullaje Motorizado','status'=>'active'],
        	['title' => 'Punto de Control','status'=>'active'],
        	['title' => 'Servicio Punto a Pie','status'=>'active'],
            ['title' => 'Patrullaje en unidad 4-112','status'=>'active'],
            ['title' => 'Servicio en Organismo de salud','status'=>'active'],
        ]);
    }
}
