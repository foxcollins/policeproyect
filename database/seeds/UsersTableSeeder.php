<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
        	'status'=>'active',
        	'pregunta'=>'¿cual fue tu primera mascota?',
        	'respuesta'=>'perro',
            'credencial'=>'1234567',
        	'password'=>Hash::make('123456'),
            'intentos'=>0,
        	'funtionary_id'=>1,
        ]);
    }
}
