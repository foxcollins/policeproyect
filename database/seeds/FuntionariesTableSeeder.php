<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class FuntionariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('funtionaries')->insert([
        	'cedula'=>1234567,
        	'name'=>"admin",
        	'lastname'=>"master",
        	'sexo'=>'masculino',
        	'nacimiento'=>"1990-01-01 00:00:00",
        	'email'=>'admin@master.com',
        	'direccion'=>'calle principal',
        	'lugar_nacimiento'=>'caicara',
        	'rango'=>'admin',
        	'avatar'=>'avatar-default.png',
        	'status'=>'active',
            'madre_status'=>'true',
            'padre_status'=>'true',
            'estatura'=>'1.80',
            'contextura'=>'mediana',
            'talla_zapato'=>41,
            'talla_camisa'=>'l',
            'talla_pantalon'=>34,
        	'graduado'=>"2010-03-06 00:00:00",
            'inicio'=>"2011-03-06 00:00:00",
            'created_at'=> new Carbon(),
            'updated_at'=>new Carbon(),

        ]);
        \DB::table('funtionaries')->insert([
            'cedula'=>1234500,
            'name'=>"carlos",
            'lastname'=>"acosta",
            'sexo'=>'masculino',
            'nacimiento'=>"1990-01-01 00:00:00",
            'email'=>'carlos@master.com',
            'direccion'=>'calle principal',
            'lugar_nacimiento'=>'caicara',
            'rango'=>'comisionado jefe',
            'avatar'=>'avatar-default.png',
            'status'=>'active',
            'madre_status'=>'true',
            'padre_status'=>'false',
            'estatura'=>'1.80',
            'contextura'=>'mediana',
            'talla_zapato'=>42,
            'talla_camisa'=>'m',
            'talla_pantalon'=>34,
            'graduado'=>"2010-03-06 00:00:00",
            'inicio'=>"2011-03-06 00:00:00",
            'created_at'=> new Carbon(),
            'updated_at'=>new Carbon(),

        ]);
        \DB::table('funtionaries')->insert([
            'cedula'=>1234501,
            'name'=>"petra",
            'lastname'=>"gomez",
            'sexo'=>'femenino',
            'nacimiento'=>"1990-01-01 00:00:00",
            'email'=>'petra@master.com',
            'direccion'=>'calle principal',
            'lugar_nacimiento'=>'caicara',
            'rango'=>'supervisor jefe',
            'avatar'=>'avatar-default.png',
            'status'=>'active',
            'madre_status'=>'true',
            'padre_status'=>'true',
            'estatura'=>'1.80',
            'contextura'=>'mediana',
            'talla_zapato'=>39,
            'talla_camisa'=>'s',
            'talla_pantalon'=>34,
            'graduado'=>"2008-03-06 00:00:00",
            'inicio'=>"2009-03-06 00:00:00",
            'created_at'=> new Carbon(),
            'updated_at'=>new Carbon(),

        ]);

        \DB::table('funtionaries')->insert([
            'cedula'=>1234568,
            'name'=>"juan",
            'lastname'=>"gomez",
            'sexo'=>'masculino',
            'nacimiento'=>"1990-03-06 00:00:00",
            'email'=>'juan@gomez.com',
            'direccion'=>'calle 2',
            'lugar_nacimiento'=>'maturin',
            'rango'=>'supervisor agregado',
            'avatar'=>'avatar-default.png',
            'status'=>'active',
            'madre_status'=>'false',
            'padre_status'=>'true',
            'estatura'=>'1.80',
            'contextura'=>'mediana',
            'talla_zapato'=>45,
            'talla_camisa'=>'xl',
            'talla_pantalon'=>36,
            'graduado'=>"2010-03-06 00:00:00",
            'inicio'=>"2011-03-06 00:00:00",
            'created_at'=>new Carbon(),
            'updated_at'=>new Carbon(),

        ]);
    }
}
