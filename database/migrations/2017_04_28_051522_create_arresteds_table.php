<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArrestedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arresteds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->text('resumen');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->integer('novelty_id')->unsigned();
            $table->foreign('novelty_id')->references('id')->on('novelties');
            $table->integer('funtionary_id')->unsigned();
            $table->foreign('funtionary_id')->references('id')->on('funtionaries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arresteds');
    }
}
