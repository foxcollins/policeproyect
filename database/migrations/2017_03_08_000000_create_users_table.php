<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->string('pregunta');
            $table->string('respuesta');
            $table->string('credencial');
            $table->string('password');
            $table->integer('intentos');
            $table->rememberToken();
            $table->integer('funtionary_id')->unsigned();
            $table->foreign('funtionary_id')->references('id')->on('funtionaries')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
