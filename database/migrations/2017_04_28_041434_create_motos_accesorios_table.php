<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotosAccesoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motos_accesorios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('estado');
            $table->integer('moto_id')->unsigned();
            $table->foreign('moto_id')->references('id')->on('motos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motos_accesorios');
    }
}
