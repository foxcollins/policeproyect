<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePadresTable extends Migration
{
    
    public function up()
    {
        Schema::create('padres', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cedula');
            $table->string('name');
            $table->string('lastname');
            $table->dateTime('fecha_nacimiento');
            $table->string('type');
            $table->integer('funtionary_id')->unsigned();
            $table->foreign('funtionary_id')->references('id')->on('funtionaries');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('padres');
    }
}
