<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuntionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funtionaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cedula')->unique();
            $table->string('name');
            $table->string('lastname');
            $table->string('sexo');
            $table->timestamp('nacimiento');
            $table->string('email');
            $table->string('lugar_nacimiento');
            $table->string('direccion');
            $table->string('rango');
            $table->string('avatar');
            $table->string('status');
            $table->string('madre_status');
            $table->string('padre_status');
            $table->dateTime('graduado');
            $table->dateTime('inicio');
            $table->string('estatura');
            $table->string('contextura');
            $table->integer('talla_zapato');
            $table->string('talla_camisa');
            $table->integer('talla_pantalon');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funtionaries');
    }
}
