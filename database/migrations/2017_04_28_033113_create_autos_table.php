<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('placa');
            $table->string('documento');
            $table->string('licencia');
            $table->string('certificado');
            $table->string('clase');
            $table->string('serial_motor');
            $table->string('serial_carroceria');
            $table->string('marca');
            $table->string('modelo');
            $table->string('tipo');
            $table->string('year');
            $table->string('color');
            $table->dateTime('fecha');
            $table->dateTime('hora');
            $table->text('observacion');
            $table->integer('propietario_id')->unsigned();
            $table->foreign('propietario_id')->references('id')->on('personas');
            $table->integer('conductor_id')->unsigned();
            $table->foreign('conductor_id')->references('id')->on('personas');
            $table->integer('novelty_id')->unsigned();
            $table->foreign('novelty_id')->references('id')->on('novelties');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autos');
    }
}
