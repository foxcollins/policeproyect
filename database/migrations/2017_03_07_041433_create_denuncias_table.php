<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDenunciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denuncias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cedula');
            $table->integer('edad');
            $table->string('estado_civil');
            $table->string('residencia');
            $table->string('profesion');
            $table->integer('phone');
            $table->text('description');
            $table->string('status');
            $table->integer('funtionary_id')->unsigned();
            $table->foreign('funtionary_id')->references('id')
            ->on('funtionaries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denuncias');
    }
}
