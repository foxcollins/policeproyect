<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoveltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novelties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->string('lugar');
            $table->text('resumen');
            $table->integer('personas');
            $table->integer('motos');
            $table->integer('autos');
            $table->dateTime('hora_inicio');
            $table->dateTime('hora_final');
            $table->dateTime('fecha');
            $table->integer('encargado_id')->unsigned();
            $table->foreign('encargado_id')->references('id')->on('funtionaries')
            ->onDelete('cascade');
            $table->integer('funtionary_id')->unsigned();
            $table->foreign('funtionary_id')->references('id')->on('funtionaries')
            ->onDelete('cascade');
            $table->integer('service_id')->unsigned();
            $table->foreign('service_id')->references('id')->on('services')
            ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novelties');
    }
}
