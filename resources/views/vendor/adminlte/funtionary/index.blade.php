@extends('adminlte::layouts.app')

@section('htmlheader_title')
	PERSONAL | listado
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
			<div class="panel-heading">
					<div class="col-md-6">
						<div class="btn-group btn-group">
							<button data-toggle="modal" data-target="#modal-registro" class="btn-group-addon btn btn-lg btn-link" title=""> Registrar <i class="fa fa-plus fa-lg"></i></button>
							
						</div>
					</div>
					<div class="col-md-6">
						<!-- search form (Optional) -->
						<form action="" method="get" class="">
						    <div class="input-group">
						        <input type="text" name="buscar" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
						      <span class="input-group-btn">
						        <button type='submit' id='search-btn' class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
						      </span>
						    </div>
						</form>
						<!-- /.search form -->
					</div>
				</div>
				<div class="col-md-12">
				<br>
					@if (count($funtionaries)>0)
						<table class="table table-condensed table-responsive table-hover">
							<thead>
								<tr>
									<th class="text-center">Cédula</th>
									<th class="text-center">Nombre</th>
									<th class="text-center">Apellido</th>
									<th class="text-center">Rango</th>
									<th class="text-center">Años de Servicios</th>
									<th class="text-right">Opciones</th>
									
								</tr>
							</thead>
							<tbody>
								@foreach ($funtionaries as $funtionary)
									<tr class="text-center">
										<td><a href="{{ url('personal') }}/{{$funtionary->id}}">{{$funtionary->cedula}}</a></td>
										<td><a href="{{ url('personal') }}/{{$funtionary->id}}">{{$funtionary->name}}</a></td>
										<td><a href="{{ url('personal') }}/{{$funtionary->id}}">{{$funtionary->lastname}}</a></td>
										<td>{{$funtionary->rango}}</td>
										<td>{{$funtionary->inicio->age}}</td>
										<td class="btn-group pull-right">
											@if (!empty($funtionary->user->id))
												<a href="" title="Usuario del Sistema" class="btn btn-xs"><i class=" fa fa-user-o fa-lg"></i></a>
											@endif
											<a href="{{ url('personal') }}/{{$funtionary->id}}" title="" class="btn btn-xs"><i class="fa fa-external-link-square fa-lg"></i></a>
											<a href="" title="Editar Personal" class="btn btn-xs"><i class="fa fa-pencil fa-lg"></i></a>
											<a href="" title="Eliminar Personal" class="btn btn-xs" > <i class="fa fa-remove fa-lg"></i></a>
											
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					@else
						<h3 class="text-center text-muted">No Existe ningun Personal Registrado <i class="fa fa-exclamation-circle"></i></h3>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-registro">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Registrar Nuevo Personal</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<h5 class="text-center">Datos Personales</h5>
						<div class="row">
							<div class="col-md-4">
							<label class="label label-default"><b>Cédula</b></label>
								<input type="text" name="" placeholder="Cédula" class="form-control">
							</div>
							<div class="col-md-4">
							<label class="label label-default"><b>Nombres</b></label>
								<input type="text" name="" placeholder="Nombres" class="form-control">
							</div>
							<div class="col-md-4">
							<label class="label label-default"><b>Apellidos</b></label>
								<input type="text" name="" placeholder="Apellidos" class="form-control">
							</div>
							<div class="col-md-4">
							<label class="label label-default"><b>Correo Electronico</b></label>
								<input type="text" name="" placeholder="ejem@dominio.com" class="form-control">
							</div>
							<div class="col-md-4">
							<label class="label label-default"><b>Sexo</b></label>
								<select name="" class="form-control">
									<option value="">Seleccionar</option>
									<option value="Masculino">Masculino</option>
									<option value="Femenino">Femenino</option>
								</select>
							</div>
							<div class="col-md-4">
							<label class="label label-default"><b>Nacionalidad</b></label>
								<select name="" class="form-control">
									<option value="V">Venezuela</option>
									<option value="E">Extranjero</option>
								</select>
							</div>
							
						</div>
						<h5 class="text-center">Datos de Procedencia</h5>
						<div class="row">
							<div class="col-md-4">
							<label class="label label-default"><b>Estado</b></label>
								<select name="" class="form-control">
									<option>actualizar esta lista en la BD</option>
									<option value="Amazonas">Amazonas</option>
									<option value="Anzuateqgui">Anzuateqgui</option>
									<option value="Apure">Apure</option>
									<option value="Aragua">Aragua</option>
									<option value="Barinas">Barinas</option>
									<option value="Bolivar">Bolivar</option>
									<option value="Carabobo">Carabobo</option>
									<option value="Cojedes">Cojedes</option>
									<option value="Distrito Capital">Distrito Capital</option>
									<option value="Delta Amacuro">Delta Amacuro</option>
									<option value="Nueva Esparta">Nueva Esparta</option>
									<option value="Miranda">Miranda</option>
									<option value="Merida">Merida</option>
									<option value="Monagas">Monagas</option>
									<option value="Sucre">Sucre</option>
									<option value="Tachira">Tachira</option>
									<option value="Trujillo">Trujillo</option>
									<option value="Portuguesa">Portuguesa</option>
									<option value="Zulia">Zulia</option>
								</select>
							</div>
							<div class="col-md-4">
								<span class="label label-default">Municipio</span>
								<select name="" class="form-control">
									<option value="">actualizar esta lista</option>
									<option value="Cedeño">Cedeño</option>
									<option value="Ezequiel Zamora">Ezequiel Zamora</option>
									<option value="Piar">Piar</option>
									<option value="Bolivar">Bolivar</option>
								</select>
							</div>
						</div>
						<h5 class="text-center">Datos de Residencia</h5>
						<div class="row">
							
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-remove fa-2x"></i></button>
					<button type="button" class="btn btn-link"><i class="fa fa-save fa-2x"></i></button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
			
@endsection
