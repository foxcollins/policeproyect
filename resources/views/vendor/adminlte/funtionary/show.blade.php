@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Perfil | {{$funtionary->name}} {{$funtionary->lastname}}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-10">
				
				{{--panel default--}}
					<div class="panel panel-default">
					  <div class="panel-heading text-center bg-gray"><b>Información Personal</b></div>
					  <div class="panel-body">
					    <div class="col-md-3">
					    	<p class="text-uppercase">Nombres:</p>
					    	<p class="text-uppercase">Apellidos:</p>
					    	<p class="text-uppercase">Cédula:</p>
					    	<p class="text-uppercase">Sexo:</p>
					    	<p class="text-uppercase">Nacimiento:</p>
					    	<p class="text-uppercase">edad:</p>
					    	<p class="text-uppercase">Rango:</p>

					    </div>
					    <div class="row text-left">
					    	<p class="text-uppercase"><b>{{$funtionary->name}}</b></p>
					    	<p class="text-uppercase"><b>{{$funtionary->lastname}}</b></p>
					    	<p class="text-uppercase"><b>{{$funtionary->cedula}}</b></p>
					    	<p class="text-uppercase"><b>{{$funtionary->sexo}}</b></p>
					    	<p class="text-uppercase"><b>{{$funtionary->nacimiento->formatLocalized('%d de %B de %Y')}}</b></p>
					    	<p class="text-uppercase"><b>{{$funtionary->nacimiento->age}} Años</b></p>
					    	<p class="text-uppercase"><b>{{$funtionary->rango}} </b> 
					    	<small><i class="fa fa-star text-yellow"></i></small>
					    	<small><i class="fa fa-star text-yellow"></i></small>
					    	<small><i class="fa fa-star text-yellow"></i></small></p>
					    </div>
					  </div>
					
					  <div class="panel-heading text-center bg-gray"><b>Datos de Residencia</b></div>
					  <div class="panel-body">
					    Panel content
					  </div>

					  <div class="panel-heading text-center bg-gray"><b>Datos de Origen</b></div>
					  <div class="panel-body">
					    Panel content
					  </div>

					</div>
					{{--end panel default--}}
				</div>
				<div class="col-md-2">
					<div class="row">
						<button type="button" data-toggle="modal" data-target="#modal-changeavatar" class="row btn-link">
							<div class="img-thumbnail">
								<img src="{{ url('img/avatar') }}/{{$funtionary->avatar}}" alt="" class="img-responsive" style="min-height: 200px; max-height: 200px; min-width: 200px; max-width: 200px;">
							</div>
						</button>
					</div>
				</div>
				<div class="col-md-2">
					<div class="row">
						<div class="list-group">
						  <a href="#" class="list-group-item">
						    Historial <i class="fa fa-history text-muted pull-right"></i>
						  </a>
						  @if (!empty($funtionary->user->id))
						  <a href="#" class="list-group-item">Usuario <i class="fa fa-user text-green pull-right"></i></a>
						  @else
						  <a href="#" class="list-group-item">Crear Usuario </a>
						  @endif
						  <a href="#" class="list-group-item">Turno Actual <i class="fa fa-clock-o fa-sm pull-right"></i></a>
						  <a href="#" class="list-group-item">Vestibulum</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
			<div class="modal fade" id="modal-changeavatar">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Close</span>
							</button>
							<h4 class="modal-title text-center">Cambiar Foto de Perfil</h4>
						</div>
						<form action="{{ url('avatar') }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="row">
								<div class="col-md-8 com-sm-8 col-lg-offset-2 col-md-offset-2">
									<h4 class="text-center text-muted">Vista previa</h4>
									<output class="img-thumbnail list" id="list">

										<img id="img-actual" src="{{ url('img/avatar') }}/{{$funtionary->avatar}}" alt="" class="img-responsive">
									</output>
								</div>
								<div class="col-md-12">
									<input type="hidden" name="_token" value="{{ csrf_token()}}">
								    <input type="file" name="avatar" class="btn btn-default btn-flat btn-sm" id="files" />
								    <input type="hidden" name="id" value="{{$funtionary->id}}">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" onclick="resetModal();" data-dismiss="modal"><i class="fa fa-remove fa-sm"></i></button>
							<button type="submit" class="btn btn-primary"><i class="fa fa-upload fa-sm"></i></button>
						</div>
						</form>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
@endsection
@section('myscripts')
<script>

		  function archivo(evt) {
			var files = evt.target.files; // FileList object
		
			// Obtenemos la imagen del campo "file".
			for (var i = 0, f; f = files[i]; i++) {
			  //Solo admitimos imágenes.
			  if (!f.type.match('image.*')) {
				continue;
			  }
		
			  var reader = new FileReader();
		
			  reader.onload = (function(theFile) {
				return function(e) {
				  // Insertamos la imagen
				  $("#img-actual").hide();
				 document.getElementById("list").innerHTML = ['<img class="img-responsive" id="img2" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
				};
			  })(f);
		
			  reader.readAsDataURL(f);
			}
		  }
		
		  document.getElementById('files').addEventListener('change', archivo, false);
		  document.getElementById('files').addEventListener('drop', archivo, false);


		  function resetModal(){
		  	$("#img2").hide();
		  	$("#img-actual").show();
		  	$("#files").val('');
		  }
	</script>
@endsection
 