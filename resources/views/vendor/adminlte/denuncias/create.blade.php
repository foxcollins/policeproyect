@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Nueva Denuncia
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				    <div class="container-fluid">
					    <div class="row form-group">
				            <div class="col-xs-12">
				                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
				                    <li class="active"><a href="#step-1">
				                        <h4 class="list-group-item-heading">Paso 1</h4>
				                        <p class="list-group-item-text">Datos del denunciante</p>
				                    </a></li>
				                    <li class="disabled"><a href="#step-2">
				                        <h4 class="list-group-item-heading">Paso 2</h4>
				                        <p class="list-group-item-text">Descripción</p>
				                    </a></li>
				                    <li class="disabled"><a href="#step-3">
				                        <h4 class="list-group-item-heading">Paso 3</h4>
				                        <p class="list-group-item-text">Datos del denunciado</p>
				                    </a></li>
				                    <li class="disabled"><a href="#step-4">
				                        <h4 class="list-group-item-heading">Paso Final</h4>
				                        <p class="list-group-item-text">Guardar denuncia</p>
				                    </a></li>    
				                </ul>
				                <h4><span class="label label-default">
				                	Los campos marcados con un (<strong class="text-danger"> * </strong>) son requeridos por el sistema 
				                </span></h4>
				            </div>
					    </div>
				    </div>	
				   
				<form class="container-fluid">

				    <div class="row setup-content" id="step-1">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="col-md-12">
				                	<form action="" method="" accept-charset="utf-8">
				                		<div class="col-md-12">
				                			<div class="col-md-4">
				                				<label for="">Cédula <b class="text-danger">*</b></label>
				                				<div class="input-group">
				                				<span class="input-group-addon">
				                						<i class="fa fa-id-card-o"></i>
				                					</span>
				                					<input type="text" name="cedula" id="cedula" class="form-control">
				                				</div>
					                		</div>
							             	<div class="col-md-4">
							             		<label for="">Nombre <b class="text-danger">*</b></label>
							             		<input type="text" name="" class="form-control">
							             	</div>
							             	<div class="col-md-4">
							             		<label for="">Apellido <b class="text-danger">*</b></label>
							             		<input type="text" name="" class="form-control">
							             	</div>
				                		</div>
				          				
				                		<div class="col-md-12">
				                		<hr>
				                			<div class="col-md-4">
				                				<label for="">Edo. Civil <b class="text-danger">*</b></label>
				                				<select name="" class="form-control">
				                					<option value="">Seleccionar</option>
				                					<option value="soltero">Soltero</option>
				                					<option value="casado">Casado</option>
				                					<option value="viudo">viudo</option>
				                					<option value="divorciado">Divorciado</option>
				                					<option value="concubino">Concubino</option>
				                				</select>
				                			</div>
				                			<div class="col-md-4">
				                				<label for="">Fecha de nacimiento <b class="text-danger">*</b></label>
				                				<div class="input-group">
				                					<span class="input-group-addon"><i class="fa fa-calendar-o fa-lg"></i></span>
				                				  	<input type="text" name="fecha_nac" class="form-control" id="fecha">
				                				</div>
				                			</div>
				                			<div class="col-md-4">
				                				<label for="">Teléfono <b class="text-danger">*</b></label>
				                				<div class="input-group">
				                					<span class="input-group-addon">
				                						<i class="fa fa-mobile-phone fa-lg"></i>
				                					</span>
				                					<input type="text" name="phone" class="form-control" id="phone">
				                				</div>
				                			</div>
				                		</div>
				                		<div class="col-md-12">
				                			<hr>
				                			<div class="col-md-4">
				                			<label for="">Sexo</label>
				                				<div class="input-group">
				                					<span class="input-group-addon">
				                						<i class="fa fa-venus-mars fa-lg"></i>
				                					</span>
				                					<select name="" class="form-control">
				                						<option value="">Seleccionar</option>
				                						<option value="Hombre">Hombre</option>
				                						<option value="Mujer">Mujer</option>
				                					</select>
				                					
				                				</div>
				                			</div>
				                			<div class="col-md-8">
				                			<label>Dirección <b class="text-danger">*</b></label>
				                				<div class="input-group">
				                					<span class="input-group-addon"><i class="fa fa-map-o fa-lg"></i></span>
				                					<input type="text" name="" class="form-control">
				                					<span class="input-group-addon"><i class="fa fa-map-marker fa-lg"></i></span>
				                				</div>
				                			</div>
				                		</div>
						             	<div class="col-md-12">
						             		<hr>
						             		<button id="activate-step-2" class="btn btn-primary btn-md pull-right">Siguiente</button>
						             	</div>
				              		</form>	
				                </div>
				                
				<!-- <form> -->

				                
				   {{--}} <div class="container-fluid col-xs-12">
				        <div class="row clearfix">
						    <div class="col-md-12 column">
							    <table class="table table-bordered table-hover" id="tab_logic">
								<thead>
									<tr >
										<th class="text-center">
											#
										</th>
										<th class="text-center">
											Name
										</th>
										<th class="text-center">
											Surname
										</th>
										<th class="text-center">
											Email
										</th>
										
										<th class="text-center">
											Gender
										</th>						
										
									</tr>
								</thead>
								<tbody>
									<tr id='addr0'>
										<td>
										1
										</td>
										<td>
										<input type="text" name='name0'  placeholder='Name' class="form-control"/>
										</td>
										<td>
										<input type="text" name='sur0' placeholder='Surname' class="form-control"/>
										</td>
										<td>
										<input type="text" name='email0' placeholder='Email' class="form-control"/>
										</td>
										<td>
				                        <select type="text" name="gender0" class="form-control">
				                            <option name="male" value="male">Male</option>
				                            <option name="Female" value="Female">Female</option>
				                            <option name="3rdgen" value="none">None</option>
				                        </select>
				                        </td>
									</tr>
				                    <tr id='addr1'></tr>
								</tbody>
							    </table>
						    </div>
					    </div>
					    <a id="add_row" class="btn btn-success pull-left">Add Row</a><a id='delete_row' class="btn btn-danger pull-right">Delete Row</a>
				    </div>--}}     
				<!-- </form> -->
				                
				            </div>
				        </div>
				    </div>

				</form>

				<form class="container-fluid">

				    <div class="row setup-content" id="step-2">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="col-md-12">
				                <label for="">Exposición del caso</label>
				                	<textarea name="" id="descripcion" class="form-control">{{old('resumen')}}</textarea>
				                </div>	
				                <div class="col-md-12">
				                	<hr>
				                	<button id="activate-step-3" class="btn btn-primary btn-md pull-right">Siguiente</button>
				                </div>
				            </div>
				        </div>
				    </div>

				</form>

				<form class="container-fluid">

				    <div class="row setup-content" id="step-3">
				        <div class="col-xs-12">
				            <div class="col-md-12 well text-center">
				        
				                <div class="col-md-12">
				                <hr>
				                <button id="activate-step-4" class="btn btn-primary pull-right btn-md">Siguiente</button>
				                </div>
				            </div>
				        </div>
				    </div>

				</form>

				<form class="container-fluid">
				    
				    <div class="row setup-content" id="step-4">
				        <div class="col-xs-12">
				            <div class="col-md-12 well text-center">
				                <h1 class="text-center"> STEP 4</h1>
				                
				<!--<form></form> -->                
				                
				            </div>
				        </div>
				    </div>

				</form>

			</div>
		</div>
	</div>
@endsection

@section('myscripts')
	<script>
		// Activate Next Step

		$(document).ready(function() {
		    
		    var navListItems = $('ul.setup-panel li a'),
		        allWells = $('.setup-content');

		    allWells.hide();

		    navListItems.click(function(e)
		    {
		        e.preventDefault();
		        var $target = $($(this).attr('href')),
		            $item = $(this).closest('li');
		        
		        if (!$item.hasClass('disabled')) {
		            navListItems.closest('li').removeClass('active');
		            $item.addClass('active');
		            allWells.hide();
		            $target.show();
		        }
		    });
		    
		    $('ul.setup-panel li.active a').trigger('click');
		    
		    // DEMO ONLY //
		    $('#activate-step-2').on('click', function(e) {
		        	$('ul.setup-panel li:eq(1)').removeClass('disabled');
		        	$('ul.setup-panel li a[href="#step-2"]').trigger('click');
		        	$(this).remove();
		        
		    })
		    
		    $('#activate-step-3').on('click', function(e) {
		        $('ul.setup-panel li:eq(2)').removeClass('disabled');
		        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
		        $(this).remove();
		    })
		    
		    $('#activate-step-4').on('click', function(e) {
		        $('ul.setup-panel li:eq(3)').removeClass('disabled');
		        $('ul.setup-panel li a[href="#step-4"]').trigger('click');
		        $(this).remove();
		    })
		    
		    $('#activate-step-3').on('click', function(e) {
		        $('ul.setup-panel li:eq(2)').removeClass('disabled');
		        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
		        $(this).remove();
		    })
		});


		// Add , Dlelete row dynamically

		     $(document).ready(function(){
		      var i=1;
		     $("#add_row").click(function(){
		      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='name"+i+"' type='text' placeholder='Name' class='form-control input-md'  /> </td><td><input  name='sur"+i+"' type='text' placeholder='Surname'  class='form-control input-md'></td><td><input  name='email"+i+"' type='text' placeholder='Email'  class='form-control input-md'></td><td><select type='text' name='gender"+i+"' class='form-control'><option name='male"+i+"' value='male'>Male</option><option name='Female"+i+"' value='Female'>Female</option><option name='3rdgen"+i+"' value='none'>None</option></select></td>");

		      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
		      i++; 
		  });
		     $("#delete_row").click(function(){
		    	 if(i>1){
				 $("#addr"+(i-1)).html('');
				 i--;
				 }
			 });

		});
		     $('#descripcion').wysihtml5({
		     	toolbar: {
		     	    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
		     	    "emphasis": true, //Italics, bold, etc. Default true
		     	    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
		     	    "html": false, //Button which allows you to edit the generated HTML. Default false
		     	    "link": true, //Button to insert a link. Default true
		     	    "image": false, //Button to insert an image. Default true,
		     	    "color": true, //Button to change color of font  
		     	    "blockquote": true, //Blockquote  
		     	    "size":'sm' //default: none, other options are xs, sm, lg
		     	  }
		     });
		     $(function () {
		         $('#fecha').datetimepicker({
		         	format:'YYYY-M-D',
		         	maxDate:new Date()
		         }).val();
		      });

		     $(document).ready(function(){
		       $('#phone').inputmask("(9999) 999-9999");  //static mask
		       $("#cedula").inputmask({"mask": "9[9].999999"});
		     });
	</script>
@endsection