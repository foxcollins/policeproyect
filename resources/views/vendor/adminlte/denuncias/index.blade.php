@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Denuncias
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
			<div class="panel-header">
				<a href="{{ url('denuncias/create') }}" title="">Crear <i class="fa fa-plus fa-lg"></i></a>
			</div>
			<div class="panel-body">
				<div class="col-md-12">
					@if (count($denuncias)>0)
						<table class="table table-striped">
							<thead>
								<tr>
									<th>header</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($denuncias as $denuncia)
									<tr>
										<td>data</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					@else
					<h3 class="text-center text-muted">No Existe Denuncias</h3>
					@endif
				</div>
			</div>
			</div>
		</div>
	</div>
@endsection
