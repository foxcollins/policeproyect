@extends('adminlte::layouts.app')

@section('htmlheader_title')
	INICIO | Principal
@endsection
@php
	use Carbon\Carbon;
	setlocale(LC_TIME, 'es_VE', 'es_VE.utf-8', 'es_VE.utf8'); # Asi es mucho mas seguro que funciones, ya que no todos los sistemas llaman igual al locale ;)
	date_default_timezone_set('America/Caracas');
	
@endphp

@section('main-content')
	<div class="container-fluid spark-screen">
		
		<div class="row">
			<div class="col-md-12">
				<div class=" col-md-8">
					<div class="col-md-12">
						<h3 class="text-center">Funcionarios de turno actualmente</h3>
						<div id="turno">
							
						</div>
						<input type="hidden" name="" id="id_turno">
						<ul class="list-group" id="contenido">
							
						</ul>
					</div>
						
				</div>
				<div class="col-md-4">
					<!-- Info Boxes Style 2 -->
					        <div class="info-box bg-navy">
					            <span class="info-box-icon"><i class="ion ion-ios-barcode-outline"></i></span>
					            @php
					            	$mesactual= new Carbon();
					            @endphp
					            <div class="info-box-content">
					              <span class="info-box-text">Novedades en <b class="">{{$mesactual->formatLocalized('%B')}}</b></span>
					              <span class="info-box-number">@if (!empty($novedadesmes))
					              	{{$novedadesmes}}
					              @else
					              	0
					              @endif</span>

					              <div class="progress">
					                <div class="progress-bar" style="width: @if (!empty($concretadas))
					                	{{$concretadas}}%
					                @else
					                	0%
					                @endif"></div>
					              </div>
					                  <span class="progress-description">
					                  <small>
					                 
					                    @if (!empty($concretadas))
					                    	{{$concretadas}}
					                    @else
					                    	0
					                    @endif% 
					                    @if (!empty($concretadas))
					                    	más que en <b class="text-capitalize">{{$mesactual->subMonth(1)->formatLocalized('%B')}}</b>
					                    @endif
					                    </small>
					                  </span>
					            </div>
					            <!-- /.info-box-content -->
					        </div>
					          <!-- /.info-box -->

					       <!-- Info Boxes Style 2 -->
					        <div class="info-box bg-blue">
					            <span class="info-box-icon"><i class="ion ion-ios-bookmarks-outline"></i></span>
					            @php
					            	$mesactual= new Carbon();
					            @endphp
					            <div class="info-box-content">
					              <span class="info-box-text">Denuncias En <b class="">{{$mesactual->formatLocalized('%B')}}</span>
					              <span class="info-box-number">6</span>

					              <div class="progress">
					                <div class="progress-bar" style="width: 50%"></div>
					              </div>
					                  <span class="progress-description">
					                    50% Satisfactorias
					                  </span>
					            </div>
					            <!-- /.info-box-content -->
					        </div>
					        <!-- /.info-box -->
					        <!-- Info Boxes Style 2 -->
					         <div class="info-box bg-light-blue">
					             <span class="info-box-icon"><i class="ion ion-ios-people-outline"></i></span>

					             <div class="info-box-content">
					               <span class="info-box-text">Detenidos en Marzo</span>
					               <span class="info-box-number">5</span>

					               <div class="progress">
					                 <div class="progress-bar" style="width: 50%"></div>
					               </div>
					                   <span class="progress-description">
					                     50% incremento en 30 días
					                   </span>
					             </div>
					             <!-- /.info-box-content -->
					         </div>
					         <!-- /.info-box -->
					    <!-- Info Boxes Style 2 -->
					        <div class="info-box bg-aqua">
					            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

					            <div class="info-box-content">
					              <span class="info-box-text">Inventory</span>
					              <span class="info-box-number">5,200</span>

					              <div class="progress">
					                <div class="progress-bar" style="width: 50%"></div>
					              </div>
					                  <span class="progress-description">
					                    50% Increase in 30 Days
					                  </span>
					            </div>
					            <!-- /.info-box-content -->
					        </div>
					        <!-- /.info-box -->

				</div>
				
			</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="_token" id="_token" value="{{ csrf_token()}}">
@endsection

@section('myscripts')
	<script>
	function consultaTurno(){
		
		var route ='/ajaxturnoactual';
		var t;
		var token = $('#_token').val();
		$.ajax({
			type: 'POST',
			dataType: 'json',
			headers: {'x-CSRF-TOKEN': token},
			url: route,
			// Mostramos un mensaje con la respuesta de PHP

			success: function(turno) {
			        /* Recorremos tu respuesta con each */
			        $(document).ajaxStart(function() { Pace.restart(); });
			        $("#turno").html('');
			        
			        if (turno != "") {
			        	$.each(turno, function(i, item) {
			        	    /* Vamos agregando a nuestra tabla las filas necesarias */
			        	    var hi=new Date(item.hora_inicial);
			        	    var format ="AM";
			        	    var hour=hi.getHours();
			        	    var min=hi.getMinutes();
			        	      if(hour>11){format="PM";}
			        	      if (hour   > 12) { hour = hour - 12; }
			        	      if (hour   == 0) { hour = 12; }  
			        	      if (min < 10){min = "0" + min;}
			        	    var hf=new Date(item.hora_final);
			        	    var formatf ="AM";
			        	    var hourf=hf.getHours();
			        	    var minf=hf.getMinutes();
			        	      if(hourf>11){formatf="PM";}
			        	      if (hourf   > 12) { hourf = hourf - 12; }
			        	      if (hourf   == 0) { hourf = 12; }  
			        	      if (minf < 10){minf = "0" + minf;}
			        	   $("#turno").append('<h4><b class="text-capitalize">Turno: '+item.title+'</b></h4><p> de <b>'+hour+':'+min+':'+format+' </b> a <b>'+hourf+':'+minf+':'+formatf+'</b></p>');
			        	   $('#id_turno').val(item.id);
			        	   ajaxdeturno();
			        	});
			        }else{
			        	$('#id_turno').val();
			        	$("#turno").append('No hay Turno para esta Hora');
			        }
			   // } //cierra el if isArray
		    }//success 
		})//ajax
	}

	function ajaxdeturno(){

		var route ='/ajaxpersonalturno';
		var id=$('#id_turno').val();
		var token = $('#_token').val();
		if (id!="") {

			$.ajax({
				type: 'POST',
				dataType: 'json',
				headers: {'x-CSRF-TOKEN': token},
				url: route,
				data:{id:id},
				// Mostramos un mensaje con la respuesta de PHP
				success: function(data) {
					$(document).ajaxStart(function() { Pace.restart(); });
				        /* Recorremos tu respuesta con each */
				        $("#contenido").html('');
				        
				        if (turno != "") {
				        	$.each(data, function(i, item) {
				        	    /* Vamos agregando a nuestra tabla las filas necesarias */
				        	   $("#contenido").append('<li class="list-group-item text-capitalize"><h4><img src="{{url('img/avatar')}}/'+item.avatar+'" class="img-thumbnail" width="40" alt=""> '+item.name+' '+item.lastname+'<i class="text-muted pull-right"><br> <small>'+item.rango+'</small></i></h4></li>');
				        	});
				        }else{

				        	$("#contenido").append('No hay personal para este turno');
				        }        
				   // } //cierra el if isArray
			    }//success 
			})//ajax
		}else{
			$("#contenido").html('');
			
		}
	}

	$(document).ready(function(){
		$(document).ajaxStart(function() { Pace.restart(); });
		consultaTurno();
		setInterval('consultaTurno()',30000);
	});
		//$(document).ajaxStart(function() { Pace.restart(); });
	</script>	
@endsection
{{--
$.each(data, function(i, item) {
    /* Vamos agregando a nuestra tabla las filas necesarias */
   $("#contenido").append('<li class="list-group-item">'+item.name+'<i class="text-muted"><br><small>'+item.rango+'</small></i></li>');
});
--}}