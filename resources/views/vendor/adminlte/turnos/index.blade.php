@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Turnos
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-link" data-toggle="modal" data-target="#modal-create">Agregar <i class="fa fa-calendar-plus-o fa-lg"></i></button>
				<button class="btn btn-link">Administrar <i class="fa fa-calendar-check-o fa-lg"></i></button>
			</div>
			<div class="col-md-12" >
			@if (count($turnos)>0)
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th class="text-center">Turno</th>
							<th class="text-center">Hora Inicio</th>
							<th class="text-center">Hora Fin</th>
							<th class="text-center">Funcionarios</th>
							<th class="text-center">Estado</th>
							<th class="text-center">Opciones</th>
						</tr>
					</thead>
					<tbody  id="table">
						@foreach ($turnos as $turno)
							<tr id="data" class="text-center
							@if ($turno->status=='inactive')
								bg-gray
							@endif
							">
								<td class="text-capitalize">{{$turno->title}}</td>
								<td>{{$turno->hora_inicial->format('h:i:s A')}}</td>
								<td>{{$turno->hora_final->format('h:i:s A')}}</td>
								<td>{{count($turno->funtionary)}}</td>
								<td class="text-capitalize"><small>{{$turno->status}}</small></td>
								<td>
									<a onclick="cargarEdit('{{$turno->id}}')"  title="" class="btn btn-link"><i class="fa fa-edit fa-lg"></i></a>
									<a href="" title="" class="btn btn-link"><i class="fa fa-ban fa-lg"></i></a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<h3 class="text-muted text-center">No existen Turnos Activos <i class="fa fa-calendar-o fa-lg"></i></h3>
			@endif

			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-create">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Crear Nuevo Turno</h4>
				</div>
				<form action="" method="post" accept-charset="utf-8" id="form-edit">
				<input type="hidden" name="_token" value="{{ csrf_token()}}">
					<div class="modal-body">
						<div class="container-fluid">
							<div class="col-md-12">
								<label for="">Titulo</label>
								<input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">
							</div>
							<div class="col-md-12">
								<hr>
							</div>
							<div class="col-md-6">
								<label for="">Hora Inicial</label>
								<div class="input-group" id="hora_inicial">
								  <input type="text" name="hora_inicial" class="form-control" {{old('hora_inicial')}}>
								  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
								</div>
							</div>
							<div class="col-md-6">
								<label for="">Hora Final</label>
								<div class="input-group" id="hora_final">
									  <input type="text" name="hora_final" class="form-control" {{old('hora_final')}}>
									  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
									</div>
							</div>
							<div class="col-md-12">
								<hr>
							</div>
							<div class="">
							      <label>
							        <input type="checkbox" value="active" name="status"> Activar
							      </label>
							      <p><i class="text-muted"><small>Seleccione para activar el turno</small></i></p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-chevron-left fa-lg"></i></button>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save fa-lg"></i></button>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" id="modal-edit">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Modificar Turno</h4>
				</div>
				<form action="" method="post" accept-charset="utf-8" onsubmit="return false;">
				<input type="hidden" name="_token_" value="{{ csrf_token()}}">
				<input type="hidden" name="" id="id_turno" >
					<div class="modal-body">
						<div class="container-fluid">
							<div class="col-md-12">
								<label for="">Titulo</label>
								<input type="text" name="title" id="title_" class="form-control" value="{{old('title')}}">
							</div>
							<div class="col-md-12">
								<hr>
							</div>
							<div class="col-md-6">
								<label for="">Hora Inicial</label>
								<div class="input-group" id="hora_inicialE">
								  <input type="text" name="hora_inicial" id="hora_inicial_" class="form-control" {{old('hora_inicial')}}>
								  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
								</div>
							</div>
							<div class="col-md-6">
								<label for="">Hora Final</label>
								<div class="input-group" id="hora_finalE">
									  <input type="text" name="hora_final" id="hora_final_" class="form-control" {{old('hora_final')}}>
									  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
									</div>
							</div>
							<div class="col-md-12">
								<hr>
							</div>
							<div class="">
							      <label>
							        <input type="checkbox"  id="status_" name="status"> Activar
							      </label>
							      <p><i class="text-muted"><small>Seleccione para activar el turno</small></i></p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-chevron-left fa-lg"></i></button>
						<button onclick="enviarFormEdit();" class="btn btn-primary"><i class="fa fa-save fa-lg"></i></button>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

@endsection
@section('myscripts')
<script>
	$(function () {
	    $('#hora_inicial').datetimepicker({
	        format: 'LT'
	    });
	    $('#hora_final').datetimepicker({
	    	format: 'LT',
	        useCurrent: false //Important! See issue #1075
	    });
	    $("#hora_inicial").on("dp.change", function (e) {
	        $('#hora_final').data("DateTimePicker").minDate(e.date);
	    });
	    $("#hora_final").on("dp.change", function (e) {
	        $('#hora_inicial').data("DateTimePicker").maxDate(e.date);
	    });
	});
	$(function () {
	    $('#hora_inicialE').datetimepicker({
	        format: 'LT'
	    });
	    $('#hora_finalE').datetimepicker({
	    	format: 'LT',
	        useCurrent: false //Important! See issue #1075
	    });
	    $("#hora_inicialE").on("dp.change", function (e) {
	        $('#hora_finalE').data("DateTimePicker").minDate(e.date);
	    });
	    $("#hora_finalE").on("dp.change", function (e) {
	        $('#hora_inicialE').data("DateTimePicker").maxDate(e.date);
	    });
	});

	function cargarEdit(id){
		var id = id;
		$('#turno_id').val(id);
		var route='/turnos/'+id+'';
		var token=$('#_token').val();

		$.ajax({
		       type:"GET",
		       dataType: 'json',
		       headers: {'x-CSRF-TOKEN': token},
		       url: route,
		       data:{id:id},
		       success: function (data) {
		       	$('#id_turno').val(data.id);
		        $('#title_').val(data.title);
		        //$('#hora_inicial_').val(data.hora_inicial);
		        var hi=new Date(data.hora_inicial);
		        var format ="AM";
		        var hour=hi.getHours();
		        var min=hi.getMinutes();
		          if(hour>11){format="PM";}
		          if (hour   > 12) { hour = hour - 12; }
		          if (hour   == 0) { hour = 12; }  
		          if (min < 10){min = "0" + min;}
		        $('#hora_inicial_').val(''+hour+':'+min+' '+format+'');
		        var hf=new Date(data.hora_final);
		        var formatf ="AM";
		        var hourf=hf.getHours();
		        var minf=hf.getMinutes();
		          if(hourf>11){formatf="PM";}
		          if (hourf   > 12) { hourf = hourf - 12; }
		          if (hourf   == 0) { hourf = 12; }  
		          if (minf < 10){minf = "0" + minf;}
		        $('#hora_final_').val(''+hourf+':'+minf+' '+formatf+'');
		        if (data.status=="active") {
		        	$('#status_').prop("checked",true);
		        }else{
		        	$('#status_').prop("checked",false);
		        }
		        $('#modal-edit').modal();
		         
		       }
		     });
	}


	function enviarFormEdit(){
		var token = $("#_token_").val();
		var id = $("#id_turno").val();
		var title = $("#title_").val();
		var route = "turnos/"+id+'';
		var horai=$("#hora_inicial_").val();
		var horaf=$("#hora_final_").val();
		if ($("#status_:checked").val()=='on') {
			var status ='active';
		}
		else{
			var status='inactive';
		}
		$("#modal-edit").modal('hide');
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		$.ajax({
		       type:"PUT",
		       dataType: 'json',
		       headers: {'x-CSRF-TOKEN': token},
		       url: route,
		       data:{id:id,
		       		title:title,
		       		hora_inicial:horai,
		       		hora_final:horaf,
		       		status:status
		       		},
		       success: function (data) {
		        if (data=='success') {
		        	swal(
		        	  'Actualizado',
		        	  'Turno actualizado correctamente',
		        	  'success'
		        	)
		        	$("#table").load("turnos/ #data");
		        }
		       }
		     });

	}
</script>
@endsection