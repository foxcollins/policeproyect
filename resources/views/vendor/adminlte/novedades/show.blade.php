@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{$novelty->service->title}}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
			<!-- Main content -->
			    <section class="invoice">
			      <!-- title row -->
			      <div class="row">
			        <div class="col-md-12 ">
			          <div class="col-md-12 page-header">
			          	<div class="col-md-9">
			          		<h3 class=" text-capitalize">
			          		  <i class="fa fa-globe"></i> {{$novelty->service->title}} - {{$novelty->lugar}}
			          		</h3>
			          	</div>
					    <div class="col-md-3 text-right">
					    	<small class=""> 
					          	<h5><i class="fa fa-calendar"> </i> {{$novelty->fecha->format('d/m/Y')}}</h5 	>
				          	</small>
				          	<small class="">
				          		<h5><i class="fa fa-clock-o"></i> {{$novelty->hora_inicio->format('h:i A')}} a {{$novelty->hora_final->format('h:i A')}}</h5>
				          	</small>
					    </div>
			          </div>
			          
			          
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- info row -->
			      <div class="row invoice-info">
			        <div class="col-sm-3 invoice-col">
			          <address><p>Oficial a Cargo:</p>
			            <strong class="text-capitalize"> {{$novelty->encargado->name}} {{$novelty->encargado->lastname}}</strong><br>

			            <i class="text-capitalize">{{$novelty->encargado->rango}}</i><br>
			          </address>
			        </div>
			        <!-- /.col -->
			       
			        <!-- /.col -->
			        <div class="col-md-3 invoice-col pull-right text-right">
			          <p>Oficiales Auxiliares:</p>
			          @if (count($novelty->noveltyassistant)>0)
			          	@foreach ($novelty->noveltyassistant as $auxiliar)
			          		<b class="text-capitalize">{{$auxiliar->name}} {{$auxiliar->lastname}}</b><br>
			          	@endforeach
			          @else
			          	Sin Oficial Auxiliar
			          @endif
			          
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- /.row -->

			      <!-- resumen -->
			      <div class="panel-body text-justify">
			      
			      <h4 class="">Resumen</h4>
			        {!!$novelty->resumen!!}	
			      </div>
			      <!-- /.row -->
			      <hr>
			      <div class="col-md-12 bg-gray-light">
			      <div class="col-md-3">
			      	<h4 class=""><small>Datos de la novedad </small><b> <i class="fa fa-arrow-right"></i></b></h4>
			      </div>
			      	<div class="col-md-3 ">

			      		<h4><a href="#" data-target="#modal-arrestadostabla" data-toggle="modal" title="LISTA DE PERSONAS ARRESTADAS" class="">{{count($novelty->personasarrestadas)}}</a>/{{$novelty->personas}} <i class="fa @if ($novelty->personas>1)
			      			fa-users
			      		@else
			      			fa-user
			      		@endif fa-sm"></i></h4>
			      	</div>
			      	<div class="col-md-3">
			      		<h4>{{$novelty->autos}} <i class="fa fa-automobile fa-sm"></i></h4>
			      	</div>
			      	<div class="col-md-3">
			      		<h4><a href="#" data-target="#modal-motosarrestadas" data-toggle="modal" title="">{{count($motos)}}</a>/{{$novelty->motos}} <i class="fa fa-motorcycle fa-sm"></i></h4>
			      	</div>
			      </div>
			      <div class="panel-heading">
			      	<div class="col-md-3">
			      		<small class="text-muted text-capitalize">Registrado por: 
			      		<a href="{{ url('personal') }}/{{$novelty->funtionary->id}}" title="">{{$novelty->funtionary->name}} {{$novelty->funtionary->lastname}}</a>
			      		</small>
			      	</div>

			      	<div class="col-md-8 pull-right text-right">
				      	<button class="btn btn-link" data-target="#modal-addpersona" data-toggle="modal">Cargar Arresto <i class="fa fa-user fa-lg"></i></button>
				      	<button class="btn btn-link" data-target="#modal-addmoto" data-toggle="modal">Agregar <i class="fa fa-motorcycle fa-lg"></i></button>
				      	<button class="btn btn-link">Agregar <i class="fa fa-automobile fa-lg"></i></button>
			      		<a href="{{ url('novedades') }}" title="Contraer" class="btn"><i class="fa fa-compress fa-lg"></i></a>
			      		<a href="{{ url('novedades') }}/{{$novelty->id}}/edit" title="" class="btn"><i class="fa fa-edit fa-lg"></i></a>
			      		<a href="" title="" class="btn"><i class="fa fa-print fa-lg"></i></a>
			      		<div class="col-md-1 pull-right">
			      			{!! Form::open(['route' => ['novedades.destroy',$novelty->id],'method'=>'DELETE' , 'id' => 'form' , 'name' => 'form']) !!}

			      				<input type="hidden" name="id" value="{{$novelty->id}}">
			      				<button title="Eliminar" id="btnenviar" type="submit" class="btn btn-link text-red"><i class="fa fa-ban fa-lg"></i></button>
			      			{{ Form::close() }}
			      		</div>
			      		


			      	</div>
			      	<br><br>
			      </div>
			    </section>
			    <!-- /.content -->

			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-addpersona">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Cargar datos de la persona arrestada</h4>
				</div>
				<form action="{{ url('/arrestados') }}" method="POST" accept-charset="utf-8">
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="_token" value="{{ csrf_token()}}">
						<input type="hidden" name="novelty_id" value="{{$novelty->id}}">
						<input type="hidden" name="cedula" id="cedula">
							<div class="col-md-12">
								<label for="">Seleccionar Persona</label>
								<select name="persona_id" id="persona_id" class="persona" style="width: 85%">
									@foreach ($personas as $persona)
										<option value="{{$persona->id}}">{{$persona->cedula}} - {{$persona->name}} {{$persona->lastname}}</option>
									@endforeach
								</select>
								<button type="button" onclick="Modaladdpersona('modal-addpersona');" class="btn btn-link"><i class="fa fa-plus fa-lg"></i></button>
							</div>
							<div class="col-md-12">
								<small>Nota: Si la persona no esta en la lista presione <b class="fa fa-plus"></b> para agregarla</small>
								<hr>
							</div>
							<div class="col-md-11">
								<label for="">Status del Arresto</label>
								<select name="status" class="form-control">
									<option value="">SELECCIONE EL STATUS</option>
									<option value="fiscalia">fiscalia</option>
									<option value="departamento">departamento</option>
								</select>
							</div>
							<div class="col-md-12">
								
								<hr>
							</div>
							<div class="col-md-12">
							<label for="">Observación</label>
								<textarea name="resumen" id="resumen" class="form-control"></textarea>
							</div>
							<br>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-chevron-left fa-lg"></i></button>
					<button type="submit" class="btn btn-link"><i class="fa fa-save fa-2x"></i></button>
				</div></form>
			</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" id="modal-nuevapersona">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Cargar datos personales al sistema</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						@include('adminlte::personas.form-create')
					</div>
				</div>
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" id="modal-arrestadostabla">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Listado de Personas arrestadas</h4>
				</div>
				<div class="modal-body">
					@if (count($arrestos)>0)
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>Cédula</th>
									<th>Nombre</th>
									<th>Apellido</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($arrestos as $arresto)
								<tr class="text-capitalize">
									<td>{{$arresto->cedula}}</td>
									<td><a href="{{ url('personas') }}/{{$arresto->persona_id}}" title="ver {{$arresto->name}} {{$arresto->lastname}}">{{$arresto->name}}</a></td>
									<td><a href="{{ url('personas') }}/{{$arresto->persona_id}}" title="ver {{$arresto->name}} {{$arresto->lastname}}">{{$arresto->lastname}}</a></td>
									<td>{{$arresto->status}}</td>
								</tr>
							@endforeach
								
							</tbody>
						</table>
					@else
						<h3>No existe arrestos en esta novedad</h3>
					@endif
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-chevron-left fa-lg"></i></button>
					
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" id="modal-motosarrestadas">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Listado de Motos Arrestadas</h4>
				</div>
				<div class="modal-body">
					@if (count($motos)>0)
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>Placa</th>
									<th>Modelo</th>
									<th>año</th>
									<th>Propietario</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($motos as $moto)
								<tr class="text-capitalize">
									<td><a href="{{ url('motos')}}/{{$moto->id}}" title="">{{$moto->placa}}</a></td>
									<td><a href="{{ url('motos')}}/{{$moto->id}}" title="">{{$moto->modelo}}</a></td>
									<td>{{$moto->year}}</td>
									<td><a href="{{ url('personas') }}/{{$moto->propietario_id}}" title="Propietario">{{$moto->propietario->name}} {{$moto->propietario->lastname}}</a></td>
									<td><a href="{{ url('motos') }}/{{$moto->id}}" title=""><i class="fa fa-expand fa-lg"></i></a></td>
								</tr>
							@endforeach
								
							</tbody>
						</table>
					@else
						<h3>No existe arrestos en esta novedad</h3>
					@endif
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-chevron-left fa-lg"></i></button>
					
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" id="modal-addmoto">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Datos de la Moto</h4>
					<small>Todos los campos con * son requeridos</small>
				</div>
				<form action="{{ url('/motos') }}" method="POST" accept-charset="utf-8">
				<input type="hidden" name="_token" value="{{ csrf_token()}}">
				<input type="hidden" name="novelty_id" value="{{$novelty->id}}">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4">
							<label>Propietario *</label>
							<select name="propietario_id" id="propietario_id" class="persona" style="width: 90%" required>
								<option value=""></option>
								@foreach ($personas as $persona)
								<option value="{{$persona->id}}">{{$persona->cedula}} - {{$persona->name}} {{$persona->lastname}}</option>
								@endforeach
							</select>
							<a href="#" title="Agregar Persona" onclick="Modaladdpropietario('modal-addmoto')"><i class="fa fa-plus fa-lg"></i></a>
						</div>
						<div class="col-md-4">
							<label>Conductor *</label>
							<select name="conductor_id" id="conductor_id" class="persona" style="width: 90%" required>
								<option value=""></option>
								@foreach ($personas as $persona)
								<option value="{{$persona->id}}">{{$persona->cedula}} - {{$persona->name}} {{$persona->lastname}}</option>
								@endforeach
							</select>
							<a href="#" title="Agregar Persona" onclick="Modaladdconductor('modal-addmoto')"><i class="fa fa-plus fa-lg"></i></a>
						</div>
						<div class="col-md-4">
							<label for="">Placa *</label>
							<input type="text" name="placa" class="form-control" required>
						</div>
						<div class="col-md-12">
							<hr>
							<label>Papeles en Regla</label>
						</div>
						<div class="col-md-3">
							<div class="checkbox-inline">
								<label><b>Documeto *:</b></label>
								<span>SI <input type="radio" name="documento" value="si" required></span>
								<span>NO <input type="radio" name="documento" value="no" required></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="checkbox-inline">
								<label><b>Licencia *:</b></label>
								<span>SI <input type="radio" name="licencia" value="si" required></span>
								<span>NO <input type="radio" name="licencia" value="no" required></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="checkbox-inline">
								<label><b>Certificadon *:</b></label>
								<span>SI <input type="radio" name="certificado" value="si" required></span>
								<span>NO <input type="radio" name="certificado" value="no" required></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="checkbox-inline">
								<label><b>Casco *:</b></label>
								<span>SI <input type="radio" name="casco" value="si" required></span>
								<span>NO <input type="radio" name="casco" value="no" required></span>
							</div>
						</div>
						<div class="col-md-4">
							<label for="">Serial del motor *</label>
							<input type="text" name="serial_motor" class="form-control" required>
						</div>
						<div class="col-md-4">
							<label for="">Serial de carroceria *</label>
							<input type="text" name="serial_carroceria" class="form-control" required>
						</div>
						<div class="col-md-4">
							<label for="">Marca *</label>
							<input type="text" name="marca" class="form-control" required>
						</div>
						<div class="col-md-4">
							<label for="">Modelo *</label>
							<input type="text" name="modelo" class="form-control" required>
						</div>
						<div class="col-md-4">
							<label for="">Año *</label>
							<div class="input-group" id="year">
							  <input type="text" name="year" class="form-control" {{old('year')}} required>
							  <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
							</div>
						</div>
						<div class="col-md-4">
							<label for="">Color *</label>
							<select name="colores[]" multiple class="form-control persona" style="width: 100%" required>
								<option value="azul">Azul</option>
								<option value="amarillo">amarillo</option>
								<option value="blanco">blanco</option>
								<option value="rojo">rojo</option>
								<option value="verde">verde</option>
								<option value="negro">negro</option>
								<option value="marron">marron</option>
								<option value="rosado">rosado</option>
								<option value="anaranjado">anaranjado</option>
								<option value="morado">morado</option>
								<option value="gris">gris</option>
								<option value="vinotinto">vinotinto</option>
							</select>
						</div>
						<div class="col-md-12">
							<hr>
						</div>
						<div class="col-md-7">
						<label for="">Observacion</label>
							<textarea name="observacion" id="observacion" class="form-control"></textarea>
						</div>
						<div class="col-md-5">
							<div class="col-md-12">
								<label for="">Fecha *</label>
								<div class="input-group" id="fecha">
								  <input type="text" name="fecha" class="form-control" {{old('fecha')}} required>
								  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
								</div>
							</div>
							<div class="col-md-12">
								<label for="">Hora *</label>
								<div class="input-group" id="hora">
								  <input type="text" name="hora" class="form-control" {{old('hora')}} required>
								  <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-chevron-left"></i></button>
					<button type="submit" class="btn btn-link"><i class="fa fa-save fa-2x"></i></button>
				</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@endsection
@section('myscripts')
	@if (Session::has('modal'))
		<input type="" id="abrirmodal" value="{{Session::get('selectId')}}">
		<script>
			$(document).ready(function(){
				$("#cedula").val({{Session::get('cedula')}});
				$("#{{Session::get('selectId')}}").append('<option value="{{Session::get('id')}}" selected>{{Session::get('cedula')}} - {{Session::get('name')}} {{Session::get('lastname')}}</option>');
				$("#{{Session::get('modal')}}").modal('show');
			})
		</script>
	@endif
<script>

	document.getElementById("btnenviar").addEventListener("click", function(event){
	    event.preventDefault();
	    swal({
	      title: "Are you sure?",
	      text: "You will not be able to recover this imaginary file!",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText: "Yes, delete it!",
	      closeOnConfirm: false
	    },
	    function(){
	    	$( "#form" ).submit();
	      //swal("Deleted!", "Your imaginary file has been deleted.", "success");
	    });
	});
	

	function Modaladdpersona(modal){
		//alert();
		$("#modalId").val(modal);
		$(".selectId").val('persona_id');
		$("#modal-addpersona").modal('hide');
		$("#modal-nuevapersona").modal('show');
	}
	function Modaladdpropietario(modal){
		$(".modalname").val(modal);
		$(".selectId").val('propietario_id');
		$("#modal-addmoto").modal('hide');
		$("#modal-nuevapersona").modal('show');
	}
	function Modaladdconductor(modal){
		$(".modalname").val(modal);
		$(".selectId").val('conductor_id');
		$("#modal-addmoto").modal('hide');
		$("#modal-nuevapersona").modal('show');
	}
</script>
<script type="text/javascript">
$(document).ready(function() {
	$(".persona").select2({
		language: "es"
	});
	$('#hora').datetimepicker({
	    format: 'LT'
	});
	$('#fecha').datetimepicker({
		format: 'Y-m-d'
	});
	$('#year').datetimepicker({
		format: 'Y'
	});
	$('#resumen').wysihtml5({
		toolbar: {
		    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
		    "emphasis": true, //Italics, bold, etc. Default true
		    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
		    "html": false, //Button which allows you to edit the generated HTML. Default false
		    "link": false, //Button to insert a link. Default true
		    "image": false, //Button to insert an image. Default true,
		    "color": true, //Button to change color of font  
		    "blockquote": false, //Blockquote  
		    "size":'sm' //default: none, other options are xs, sm, lg
		  }
	});
	$('#observacion').wysihtml5({
		toolbar: {
		    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
		    "emphasis": true, //Italics, bold, etc. Default true
		    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
		    "html": false, //Button which allows you to edit the generated HTML. Default false
		    "link": false, //Button to insert a link. Default true
		    "image": false, //Button to insert an image. Default true,
		    "color": true, //Button to change color of font  
		    "blockquote": false, //Blockquote  
		    "size":'sm' //default: none, other options are xs, sm, lg
		  }
	});
});
</script>
@endsection