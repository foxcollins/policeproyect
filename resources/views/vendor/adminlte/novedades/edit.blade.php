@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Actualizar Novedad
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<h3>Actualizar Novedad #{{$novelty->id}}</h3>
				<hr>
				<form action="{{ url('novedades') }}/{{$novelty->id}}" method="post" accept-charset="utf-8">
				<input name="_method" type="hidden" value="PUT">
					<div class="col-md-11 col-md-offset-1">					
							<div class="col-md-4">
								<label for=""><b>Tipo De Servicio</b></label>
								<select name="service" class="form-control" id="service">
									<option value="{{$novelty->service->id}}" selected>{{$novelty->service->title}}</option>
									@foreach ($services as $service)
									<option value="{{$service->id}}">{{$service->title}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-3">
								<label for=""><b>Fecha</b></label>
								<div class="form-group">
								    <div class='input-group date' id='fecha'>
								        <input type='text' name="fecha" class="form-control" data-date-end-date="0d" value="{{$novelty->fecha}}" />
								        <span class="input-group-addon">
								            <span class="glyphicon glyphicon-calendar"></span>
								        </span>
								    </div>
								</div>
							</div>
							<div class="col-md-2">
								<label for=""><b>Hora Inicio</b></label>
								<div class="form-group">
								    <div class='input-group date' id='horainicial'>
								        <input type='text' name="horainicial" class="form-control" value="{{$novelty->hora_inicio}}" />
								        <span class="input-group-addon">
								            <span class="glyphicon glyphicon-time"></span>
								        </span>
								    </div>
								</div>
							</div>
							<div class="col-md-2">
								<label for=""><b>Hora Final</b></label>
								<div class="input-group" id="horafinal">
								  <input type="text" name="horafinal" class="form-control" value="{{$novelty->hora_final}}">
								  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
								</div>
							</div>
							
							<div class="col-md-4">
								<label for=""><b>Ubcación / Lugar</b></label>
								<input type="text" name="lugar" class="form-control" value="{{$novelty->lugar}}">
							</div>
							<div class="col-md-4">
								<label for=""><b>Funcionario encargado</b></label>
								<select name="encargado" id="encargado" class="form-control">
									<option value="{{$novelty->encargado->id}}">{{$novelty->encargado->name}} {{$novelty->encargado->lastname}}</option>
									@foreach ($funtionaries as $funtionary)
									<option value="{{$funtionary->id}}">{{$funtionary->name}} {{$funtionary->lastname}}</option>
									@endforeach
									
								</select>
							</div>
							<div class="col-md-4">
								<label for=""><b>Funcionarios Auxiliares</b></label>
								<select name="auxiliares[]" id="auxiliares" class="form-control" multiple="multiple">
									@foreach ($funtionaries as $funtionary)
										@if (count($novelty->noveltyassistant)>0)
											@foreach ($novelty->noveltyassistant as $assistant)
												<option value="{{$funtionary->id}}"
												@if ($funtionary->id==$assistant->id)
													selected="selected" 
												@endif
												>{{$funtionary->name}} {{$funtionary->lastname}}</option>
											@endforeach
										@else
											<option value="{{$funtionary->id}}">{{$funtionary->name}} {{$funtionary->lastname}}</option>
										@endif
									@endforeach
									
								</select>
							</div>
							<div class="col-md-9">
							<label for=""><b>Resumen</b></label>
								<textarea name="resumen" id="resumen" class="form-control" >{!!$novelty->resumen!!}</textarea>		
							</div>
							<div class="col-md-3 bg-gray-light">
							<p class="text-center"><b>Datos de Rutina</b></p>
							
							<br>
								<div class="col-md-12">
								<label for=""><small>Personas</small></label>
									<div class="form-group">
									    <div class='input-group'>
									        <input type='number' min="0" name="personas" class="form-control" value="{{$novelty->personas}}"  />
									        <span class="input-group-addon">
									            <span class="fa fa-users fa-lg"></span>
									        </span>
									    </div>
									</div>
								</div>
								<div class="col-md-12">
								<label for=""><small>Motos</small></label>
									<div class="form-group">
									    <div class='input-group'>
									        <input type='number'  min="0" name="motos" class="form-control" value="{{$novelty->autos}}"  />
									        <span class="input-group-addon">
									            <span class="fa fa-motorcycle fa-lg"></span>
									        </span>
									    </div>
									</div>
								</div>
								<div class="col-md-12">
								<label for=""><small>Autos</small></label>
									<div class="form-group">
									    <div class='input-group'>
									        <input type='number' name="autos"  min="0" class="form-control" value="{{$novelty->motos}}"  />
									        <span class="input-group-addon">
									            <span class="fa fa-automobile fa-lg"></span>
									        </span>
									    </div>
									</div>
								</div>
							</div>
							<input type="hidden" name="id" value="{{$novelty->id}}">
							<div class="col-md-12 ">
							<input type="hidden" name="_token" value="{{ csrf_token()}}">
								<button type="submit" class="btn btn-link pull-right"><i class="fa fa-save fa-2x"></i></button>
							</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	

@endsection

@section('myscripts')

	<script type="text/javascript">
		$(function () {
		    $('#fecha').datetimepicker({
		    	format:'YYYY-M-D',
		    	maxDate:new Date()
		    }).val();
		 });

	    $(function () {
	        $('#horainicial').datetimepicker({
	            format: 'LT'
	        });
	        $('#horafinal').datetimepicker({
	        	format: 'LT',
	            useCurrent: false //Important! See issue #1075
	        });
	        $("#horainicial").on("dp.change", function (e) {
	            $('#horafinal').data("DateTimePicker").minDate(e.date);
	        });
	        $("#horafinal").on("dp.change", function (e) {
	            $('#horainicial').data("DateTimePicker").maxDate(e.date);
	        });
	    });
	    $('#resumen').wysihtml5({
	    	toolbar: {
	    	    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
	    	    "emphasis": true, //Italics, bold, etc. Default true
	    	    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
	    	    "html": false, //Button which allows you to edit the generated HTML. Default false
	    	    "link": true, //Button to insert a link. Default true
	    	    "image": false, //Button to insert an image. Default true,
	    	    "color": true, //Button to change color of font  
	    	    "blockquote": true, //Blockquote  
	    	    "size":'sm' //default: none, other options are xs, sm, lg
	    	  }
	    });
	   
	   $(document).ready(function() {
	     $("#service").select2();
	   });
	   $(document).ready(function() {
	     $("#encargado").select2();
	   });
	   $(document).ready(function() {
	     $("#auxiliares").select2({
	     	tags: true,
	     });
	   });
	</script>
@endsection
