@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Novedades
@endsection
@php
@endphp

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
		<div class="col-md-12">
			<div class="panel-heading">
				<div class="col-md-3">
					<a href="{{ url('novedades/create') }}" class="btn btn-link" style="text-decoration: none;">Registrar <i class="fa fa-plus fa-lg"></i></a>
					<a href="" title="historial de novedades eliminadas">Historial <i class="fa fa-archive fa-lg"></i></a>
				</div>
				<form action="{{ url('/novedades') }}" method="get" accept-charset="utf-8">
					<div class="col-lg-4">
					   <div class="input-group">
					   <span class="input-group-addon">
					    	<i class="fa fa-calendar-check-o fa-lg"></i>
					   </span>
					    	<select name="type" id="type" class="form-control" onchange="selector(this);">
					    	 <option value="">Selecciona una opción</option>
					    	 <option value="dia">Por Fecha</option>
					    	 <option value="mes">Por mes</option>
					    	 <option value="anual">Por Año</option>
					    	</select>
					   </div><!-- /input-group -->  
					</div><!-- /.col-lg-4 -->
					<div class="col-md-4">
					 <div class="input-group" id="divdata">
					  	<input type="text" id="data" name="data" class="form-control" disabled="disabled" placeholder="">
					  	<span class="input-group-btn">
					  	  <button class="btn btn-default" type="submit"><i class="fa fa-search fa-lg"></i></button>
					  	</span>
					 </div><!-- /input-group -->
					</div>
				</form>
			</div><br>
			<hr>
		</div>

		<div class="col-md-11">
		<h6 class="text-muted">{{count($novedades)}} <i>Resultados</i>
		@if (isset($_GET['type']) and !empty($_GET['type']) )
			-
			@if ($_GET['type']=='dia')
				Busqueda por Fecha
			@endif
			@if ($_GET['type']=='mes')
				Busqueda por Mes
			@endif
			@if ($_GET['type']=='dia')
				Busqueda por Año
			@endif
		@endif
		</h6>
			@if (count($novedades)>0)
				<ul class="timeline">
				    @foreach ($novedades as $novedad)
				    	<li>
				    	  <div class="timeline-badge primary"><i class="fa fa-file-o"></i>
				    	  </div>

				    	  <div class="timeline-panel">
				    	  <div class="">
				    	  	<h4 class="text-capitalize">#{{$novedad->number}}</h4>
				    	  </div>
				    	  	<div class="col-md-2 badge label label-default pull-right">
				    	  		<b class="text-capitalize">{!!$novedad->fecha->formatLocalized(' %d de %B de %Y')!!}</b>
				    	  	</div>
				    	    <div class="timeline-heading">
					    	    <div class="col-md-12">
					    	    	<div class="col-md-8">
					    	    			<h3 class="timeline-title text-capitalize">
					    	    		  		<b>{{$novedad->service->title}}</b> - <i class="text-muted text-capitalize"> {{$novedad->lugar}}</i>
					    	    		  	</h3>
					    	      			<p class="">
					    	      				<i class="text-muted">
					    	      					<i class="glyphicon glyphicon-time"></i> 
					    	      					{{$novedad->hora_inicio->format('h:i A')}} -
					    	      					{{$novedad->hora_final->format('h:i A')}}
					    	      				</i>
					    	      			</p>

					    	    	</div>
					    	      	<div class="col-md-12 ">
					    	      		<div class="col-md-10">
					    	      			
					    	      			<p class="text-capitalize">
					    	      				Oficial a cargo: <a href="{{ url('personal') }}/{{$novedad->encargado->id}}" title="Ir al perfil">{{$novedad->encargado->name}} {{$novedad->encargado->lastname}}</a>
					    	      			</p>
					    	      		</div>
					    	      		<div class="col-md-2 text-right">
					    	      			<p class="text-capitalize">
					    	      			@if (count($novedad->noveltyassistant)>1)
					    	      				<i class="fa fa-users"></i> {{count($novedad->noveltyassistant)}} Auxiliar
					    	      			@else
					    	      				@if (count($novedad->noveltyassistant)==1)
					    	      					<i class="fa fa-user"></i> {{count($novedad->noveltyassistant)}} Auxiliar
					    	      				@else
					    	      					<small><i class="fa fa-user"></i> Sin Auxiliar</small>
					    	      				@endif
					    	      			@endif
					    	      			</p>
					    	      			
					    	      		</div>
					    	      	</div>
					    	    </div>   
				    	    </div> 
				    	    	<hr>
				    	    <div class="timeline-body">
				    	    <h5 class="text-center"><b>Resumen</b></h5>
				    	      <div class="text-justify">
				    	      	{!!str_limit($novedad->resumen,300)!!}
				    	      </div>
				    	      <hr>
				    	      	<div class="col-md-3">
				    	      		<small class="text-muted text-capitalize">Registrado por: 
				    	      		<a href="{{ url('personal') }}/{{$novedad->funtionary->id}}" title="">{{$novedad->funtionary->name}} {{$novedad->funtionary->lastname}}</a>
				    	      		</small>
				    	      	</div>
				    	      	<div class="col-md-3 pull-right text-right">
				    	      		<a href="{{ url('novedades') }}/{{$novedad->id}}" title="Abrir" class="btn"><i class="fa fa-expand fa-lg"></i></a>
				    	      		<a href="{{ url('novedades') }}/{{$novedad->id}}/edit" title="Actualizar" class="btn"><i class="fa fa-edit fa-lg"></i></a>
				    	      		<div class="col-md-2 pull-right">
				    	      			{!! Form::open(['route' => ['novedades.destroy',$novedad->id],'method'=>'DELETE' , 'id' => 'form' , 'name' => 'form']) !!}

				    	      				<input type="hidden" name="id" value="{{$novedad->id}}">
				    	      				<button title="Eliminar" id="btnenviar" type="submit" class="btn btn-link text-red"><i class="fa fa-ban fa-lg"></i></button>
				    	      			{{ Form::close() }}
				    	      		</div>
				    	      	</div>
				    	      	
				    	    </div>
				    	    
				    	  </div>
				    	</li>
				    @endforeach
				</ul>
				<div class="text-center">
					{{$novedades->links()}}
				</div>
			@else
			<h3 class="text-center text-muted">No hay novedades <i class="fa fa-frown-o"></i></h3>
			@endif
		</div>
		</div>
	</div>

			
@endsection

@section('myscripts')
	<script type="text/javascript">
		function selector(e){

				//$('#divdata').load('/novedades #divdata');
				//$('#divdata').send(null);
				var type=$('#type').val();
				if (type=='dia') {
					//el campo data se transforma en solo los calendario de un dia
					
					$('#data').datetimepicker({
						format:'YYYY-M-D',
						maxDate:new Date()
					}).val();
					$('#data').removeAttr('disabled');
					
					
				}
				if (type=='mes') {
					//el campo data se transforma en solo los meses
					$('#data').datetimepicker({
						format:'YYYY-M',
						maxDate:new Date()
					}).val();
					
					$('#data').removeAttr('disabled');
				}
				if (type=='anual') {
					//el campo data se transforma en solo años
					
					$('#data').datetimepicker({
					 format:'YYYY',
					 maxDate:new Date()
					}).val();
					$('#data').removeAttr('disabled');
				}
		}
		

		document.getElementById("btnenviar").addEventListener("click", function(event){
		    event.preventDefault();
		    swal({
		      title: "Are you sure?",
		      text: "You will not be able to recover this imaginary file!",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Yes, delete it!",
		      closeOnConfirm: false
		    },
		    function(){
		    	$( "#form" ).submit();
		      //swal("Deleted!", "Your imaginary file has been deleted.", "success");
		    });
		});
		
	</script>
@endsection