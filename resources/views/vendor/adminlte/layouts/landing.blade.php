<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ url('favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}" type="image/x-icon"/>
    <meta name="description" content="">

    <title>LOGIN</title>

    <!-- Custom styles for this template -->
  <link href="{{ asset('/css/all.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/login.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ url('css/sweetalert.css') }}">
  <link href="{{ asset('/css/themes/twitter/twitter.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="hold-transition login-page">
  <div class="container-fluid">
    <div class="row">
           <div class="col-md-12" id="box">
               
               <div class="wrap">
                   <div class="col-md-12 ">
                     <div class="col-md-2 col-md-offset-5">
                       <img src="{{ url('img/logo1.png') }}" alt="" class="img-responsive">
                     </div>
                   </div>
                   <form action="{{ url('/login') }}" method="POST" class="login" id="datos">
                   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                   <input type="text" id="cedula" placeholder="Credencial"  class="text-center" />
                   <input type="password" id="password" placeholder="Password"  class="text-center hidden" />
                   <button type="button" id="btn-entrar" onclick="chekcedula();" class="btn btn-default bg-navy btn-group-justified">ENTRAR</button>
                   <button type="button" id="btn-confirmar" onclick="authenticate();" class="hidden btn btn-default bg-navy btn-group-justified">CONFIRMAR</button>
                   <div class="">
                       <div class="row">
                           <div class="col-md-12 text-center text-muted">
                               <p id="p-msg">Igrese su número de cédula</p>
                           </div>
                       </div>
                       
                   </div>
                   </form>
               </div>
           </div>
       </div>
  </div>


</html>
<!-- jQuery 2.2.3 -->
<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('bootstrap/js/all.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/sweetalert.min.js') }}" type="text/javascript"></script>

 <!-- Include this after the sweet alert js file -->
    @include('sweet::alert')
  <script>
  $(document).ready(function(){
    $("#cedula").focus();

    $("#cedula").keypress(function(e){
      if (e.which==13) {
        chekcedula();
      }
    })
    $("#password").keypress(function(e){
      if (e.which==13) {
        authenticate();
      }
    })
  });
  function chekcedula(){
    var cedula = $("#cedula").val();
    var route = "/login";
    var token = $("#_token").val();

    if (cedula=="") {
      $("#p-msg").html('Debe ingresar su credencial de usuario');
      $("#p-msg").addClass('alert alert-warning');
    }else{
      $.ajax({
        url:route,
        headers: {'x-CSRF-TOKEN': token},
        type:'POST',
        dataType: 'json',
        data: {cedula: cedula},
        success: function(resp){      
          if (resp == "success") {
            $("#cedula").addClass('hidden');
            $("#btn-entrar").addClass('hidden');
            $("#password").removeClass('hidden');
            $("#btn-confirmar").removeClass('hidden');
            $("#p-msg").html('Ingresar Contraseña');
            $("#p-msg").removeClass('alert-warning alert alert-danger');
            $("#password").focus();


          }
          if (resp=="noexiste") {
            //alert('Credenciales no existen');
            $("#p-msg").removeClass('alert-warning');
            $("#p-msg").html('Credenciales no existen en el sistema');
            $("#p-msg").addClass('alert alert-danger');
            swal({
            title: "No existe!",
            text: "Credenciales incorrectas",
            type: "message",
            confirmButtonText: "ok"
          });
          }
          if (resp=="nouser") {
            //alert('Credenciales no existen');
            $("#p-msg").removeClass('alert-danger');
            $("#p-msg").html('Credenciales no autorizadas para entrar al sistema');
            $("#p-msg").addClass('alert alert-warning');
          } 
          if (resp=="bloqueado") {
            //alert('Credenciales no existen');
            $("#p-msg").removeClass('alert-warning');
            $("#p-msg").html('Usuario Bloqueado, pongase en contacto con el supervisor del sistema');
            $("#p-msg").addClass('alert alert-warning');
          }  
        } 
      
      });
    }
  }

  function authenticate(){
    var cedula = $("#cedula").val();
    var password = $("#password").val();
    var route = "/loginpassword";
    var token = $("#_token").val();

    $.ajax({
      url:route,
      headers: {'x-CSRF-TOKEN': token},
      type:'POST',
      dataType: 'json',
      data: { cedula:cedula,
              password: password},
      success: function(resp,intentos){ 
        if (resp == "success") {
          window.location="/home";


        }if (resp.error == "error"){
         recargarpasswordincorrecta(resp.int);
         $("#cedula").focus();
         
        }
        
      } 
    
    });
  }
  
  function recargarpasswordincorrecta(intentos){

      var refreshId = setTimeout(function() {
        $('#datos').load('#box' +  ' #datos');
        $("#cedula").focus();
    }, 2000);
    $.ajaxSetup({ cache: false }); 
    $("#p-msg").html('Contraseña Incorrecta, usted lleva '+intentos+' de 3 disponible');
    $("#p-msg").addClass('alert alert-warning');
    $("#cedula").focus();
  }
  </script> 
  
  
  </body>
 