<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar ">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ url('img/avatar/') }}/{{Auth::user()->funtionary->avatar}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info text-capitalize">
                    <p>{{ Auth::user()->funtionary->name }} {{ Auth::user()->funtionary->lastname }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Conectado</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            
            <!-- Optionally, you can add icons to the links -->
            <li class=""><a href="{{ url('home') }}"><i class='fa fa-dashboard  fa-lg'> </i> <span> <b> INICIO</b></span></a></li>
            <li ><a href="{{ url('/personal') }}"><i class='fa fa-group fa-lg'> </i> <span><b> PERSONAL</b></span></a></li>
            <li ><a href="{{ url('/denuncias') }}"><i class='fa fa-bullhorn fa-lg'> </i> <span> <b> DENUNCIAS</b></span></a></li>
            <li ><a href="{{ url('/novedades') }}"><i class='fa fa-paste fa-lg'> </i> <span> <b> NOVEDADES</b></span></a></li>
            <li ><a href="{{ url('services') }}"><i class='fa fa-street-view fa-lg'> </i> <span> <b> SERVICIOS</b></span></a></li>
            <li ><a href="#"><i class='fa fa-crosshairs fa-lg'> </i> <span> <b> CITACIONES</b></span></a></li>
            <li ><a href="{{ url('/turnos') }}"><i class='fa fa-calendar fa-lg'> </i> <span> <b> TURNOS</b></span></a></li>
            
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
