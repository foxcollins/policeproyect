<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="https://github.com/acacha/adminlte-laravel"></a><b>POLICIA DE MONAGAS</b></a>
    </div>
    <!-- Default to the left -->
    <div class="col-md-4 text-left">
    	Laravel {{ App::VERSION() }}
    </div>
    <strong>Copyright &copy; 2017</strong> 
</footer>

