<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/js/dropzone.js') }}"></script>
<script src="{{asset('/js/moment.js')}}"></script>
<script src="{{asset('/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}" charset="utf-8"></script>
<script src="{{asset('plugins/timepicker/bootstrap-timepicker.js')}}" charset="utf-8"></script>
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" charset="utf-8"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}" charset="utf-8"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script> 
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script data-pace-options='{ "ajax": false }' src="{{ asset('js/pace.min.js') }}"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
    
</script>
@yield('myscripts')
@show
@include('sweet::alert')

