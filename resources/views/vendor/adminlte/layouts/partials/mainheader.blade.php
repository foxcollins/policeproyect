<!-- Main Header -->

<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="{{ url('img/mini-logo.png') }}" alt=""></span>
        <!-- logo for regular state and mobile devices -->
        <img src="{{ url('img/mini-logo-2.png') }}" alt="">
        <span class="logo-lg"><b>POLICIA DE MONAGAS</b> </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ url('img/avatar/') }}/{{Auth::user()->funtionary->avatar}}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->funtionary->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            {{--<li class="user-header">
                                <img src="" class="img-circle" alt="User Image" />
                                <p>
                                    {{ Auth::user()->funtionary->name }}
                                    <small>{{ trans('adminlte_lang::message.login') }} Nov. 2012</small>
                                </p>
                            </li>--}}
                            <!-- Menu Body -->
                            {{--<li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.followers') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.sales') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.friends') }}</a>
                                </div>
                            </li>--}}
                            <!-- Menu Footer-->
                            <li class="">
                                <ul class="list-group text-right">
                                    <li class="list-group-item">
                                        <a href="{{ url('/settings') }}" class="btn btn-link">{{ trans('adminlte_lang::message.profile') }} <i class="fa fa-gear fa-lg"></i></a>
                                    </li>
                                    <li class="list-group-item">
                                        <form id="" action="{{ url('/logout') }}" method="POST" style="">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-link">Cerrar Sesión <i class="fa fa-sign-out fa-lg"></i></button>
                                        </form>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                @endif

                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
