@extends('adminlte::layouts.app')

@section('htmlheader_title')
	SERVICIOS
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<div class="panel-heading">
					<div class="col-md-6">
						<div class="btn-group btn-group">
							<button data-toggle="modal" data-target="#modal-registro" class="btn-group-addon btn btn-lg btn-link" title="Agregar Nuevo"> Agregar Servicio <i class="fa fa-plus fa-lg"></i></button>	
						</div>
					</div>
				</div>
				<div class="col-md-6 col-md-offset-1">
					@if (!empty($services))
						<table class="table table-condensed">
							<caption>Listado de Servicios que brinda el departamento</caption>
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Servicio</th>
									<th class="text-center">Estado</th>
									<th class="text-center">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($services as $service)
								<tr class="text-center
								@if ($service->status!="active")
									bg-gray
								@endif
								">
									<td>{{$service->id}}</td>
									<td class="text-capitalize">{{$service->title}}</td>
									<td>
										@if ($service->status=="active")
											<button class="btn btn-link text-green" onclick="changeStatus('{{$service->id}}')"><i class="fa fa-check-square-o fa-lg"></i></button>
										@else
											<button class="btn btn-link text-greis" onclick="changeStatus('{{$service->id}}')"><i class="fa fa-square-o fa-lg"></i></button>
										@endif
									</td>
									<td>
										<button title="Editar servicio" class="btn btn-link" onclick="editService('{{$service->id}}')"><i class="fa fa-edit fa-lg"></i></button>
										<button title="Eliminar servicio" class="btn btn-link text-red"><i class="fa fa-ban fa-lg"></i></button>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-registro">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Registrar Servicio</h4>
				</div>
				<form action="{{ url('/services') }}" method="post" accept-charset="utf-8">
				<div class="modal-body">
					<label for="">Titulo</label>
					<input type="tetxt" name="title" class="form-control">
					<input type="hidden" name="_token" value="{{ csrf_token()}}">
				</div>
				<div class="modal-footer">
					
					<button type="submit" class="btn btn-link"><i class="fa fa-save fa-2x"></i></button>
				</div></form>
			</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
		<div class="modal fade" id="modal-edit">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title">Editar Servicio</h4>
					</div>
				
				{!!Form::open(['url'=>'services/update','method'=>'PUT'])!!}
					<div class="modal-body">
						<label for="">Titulo</label>
						<input type="hidden" name="id" id="id">
						<input type="hidden" name="_method" value="PUT">
						<input type="text" name="title" id="title" class="form-control">
						<input type="hidden" name="_token" value="{{ csrf_token()}}">
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-link"><i class="fa fa-refresh fa-2x"></i></button>
					</div>
				{!!Form::close()!!}
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

@endsection

@section('myscripts')
	<script>
		function editService(id){
			var route= 'services/'+id+'';
			var id = id;
			$.ajax({
	            type: 'GET',
	            url: route,
	            data: id,
	            // Mostramos un mensaje con la respuesta de PHP
	            success: function(data) {
	            	$('#id').val(data.id);
	                $('#title').val(data.title);
	                $('#modal-edit').modal();
            	}
        	})
			
		}


		function changeStatus(id){
			var id = id;
			swal({
			  title: 'Desea Cambiar el estado?',
			  text: "Se cambiara el estado del servicio!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si, Cambiar ahora',
			  cancelButtonText: 'No, Cancelar',
			  confirmButtonClass: 'btn btn-success btn-sm',
			  cancelButtonClass: 'btn btn-danger btn-sm',
			  buttonsStyling: false
			}).then(function (isConfirm) {
			  if (isConfirm) {
			  	var route= '/service/changestatus/'+id+'';
			  		$.ajax({
			  			type: 'GET',
			  			url: route,
			  			data: id,
			  			// Mostramos un mensaje con la respuesta de PHP
			  			success: function(data) {
			  			    //actualizar
			  			    location.reload();
			  		    }//success
			  		})//ajax
			  }
			})
		}
	</script>
@endsection

{{--
var route= '/service/changestatus/'+id+'';
	$.ajax({
		type: 'GET',
		url: route,
		data: id,
		// Mostramos un mensaje con la respuesta de PHP
		success: function(data) {
		    //actualizar
		    location.reload();
	    }//success
	})//ajax
--}}
