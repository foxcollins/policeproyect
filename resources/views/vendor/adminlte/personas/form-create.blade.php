<form action="{{ url('personas') }}" method="POST" accept-charset="utf-8">
	<div class="col-md-12">
		<input type="hidden" name="modal" class="modalname" id="modalId">
		<input type="hidden" name="selectId" class="selectId">
		<div class="col-md-4">
			<label for="">Cedula</label>
			<input type="text" name="cedula" id="cedula" class="form-control">
		</div>
		<div class="col-md-4">
			<label for="">Nombre</label>
			<input type="text" name="name" id="lastname" class="form-control">
		</div>
		<div class="col-md-4">
			<label for="">Apellido</label>
			<input type="text" name="lastname" id="name" class="form-control">
		</div>
		<div class="col-md-4">
			<label for="">Sexo</label>
			<select name="sexo" class="form-control">
				<option value="">Seleccionar</option>
				<option value="Masculino">Masculino</option>
				<option value="Femenino">Femenino</option>
			</select>
		</div>
		<div class="col-md-4">
			<label for="">Dirección</label>
			<input type="text" name="direccion" class="form-control">
		</div>
		<div class="col-md-4">
			<label for="">Fecha de Nacimiento</label>
			<input type="text" name="fecha_nacimiento" class="form-control">
		</div>
	</div>
	<div class="modal-footer">
	<input type="hidden" name="_token" value="{{ csrf_token()}}">
		<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-chevron-left fa-lg"></i></button>
		<button type="submit" class="btn btn-link"><i class="fa fa-save fa-2x"></i></button>
	</div>
</form>



